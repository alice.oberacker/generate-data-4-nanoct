# generate-data-4-nanoct
# Data generation of simulated nano-CT samples
***

# Dataset
This package can create a dataset of simulated Nano-CT data including phantoms, sinograms and sinograms with a temporal noise, called 'perturbed sinograms'.

Phantoms can consist of rectangles and ellipses which can again include several rectangles or ellipses with different gray values representing the density. 
Sinograms are calculated using the Radon transform.
Different types of temporal noise can be added:
- overlapping damped sinus waves
- linear shifts
- random noise from normal distribution

![Samples](img/samples.png)

[//]: # (![Phantoms]&#40;img/phantoms.png&#41;)

[//]: # (![Sinograms and perturbed Sinograms]&#40;img/sinograms.png&#41;)

## Noise
Taking measurements in Nano-CT is very time-consuming, because the radiation source has to rotate around the object. In medical applications this means the heartbeat or breathing of the patient influeces the imaging. In Nano-CT the noise comes from movement between the object and the radiation source.
This movement is caused by minor vibrations in the room or by the scanner itself.

This is an example of damped sinus waves in x and y direction and a small random noise rotating the object:

<img src="img/sequence_perturbed_1.png" width="400" height="300" />

## Use case
Due to the noise, applying standard reconstruction methods to this inverse problem leads to motion artifacts in the backprojections [[1]](#1).
The purpose of creating these types of data is to train machine learning models to learn the phantoms from noisy sinograms.

[//]: # (This is called an inverse problem, or due to the temporal nature of the noise, a dynamic inverse problem. )

You can find a dataset generated with this code [here](https://zenodo.org/record/8123499) and the results of learned reconstructions as well as non-learned iterative reconstruction methods [[2]](#2).

[//]: # ([here]&#40;https://arxiv.org/abs/2307.10474&#41;.)





# How to run
## Installation
There are two options to install the python packages provided: a singularity image or anaconda environments.
### Singularity
A definition file has been provided in install/singularity.def. With this .def file you can create a Singularity image that installs all necessary packages in an anaconda environment. From this file you could also extract the install commands for an Anaconda enviroment without the intermediate step of using Singularity. 

It is also possible to download the singularity image with this command:
`singularity pull library://aoberac/nanoct/gendata`
### Anaconda
There are two yml files provided to install the environments for the weights generation (weights.yml) and the data generation (gendata.yml).

## Run Scripts

There are two main scripts for generating the dataset.
1. **generateWeightMatrixASTRA.py**: Create a weight file to calculate the Radon transformation (phantom &rarr; sinogram)
2. **generateSamples.py**: Create phantoms, noise, sinograms and perturbed sinograms

Optional:
3. **generateSinoFromPhantoms.py**: Generate sinograms for a different type of geometry (parallel or fan) using the same phantoms and noise as in an existing dataset.

The resulting file structure looks as follows:

![Directory structure](img/directories.png)

### generateWeightMatrixASTRA
#### Parameters
- path: Path of data directory
- experiment_id: Id of experiment, geometry settings defined in gendata/Setup_parallel.py or gendata/Setup_fanflat.py 
- geometry: parallel or fan
#### Results
experiment#[experiment_id]/weightMatrix/[K]_ angle_ [P]_ detectors_ [R]_ res.npz

### generateSamples
#### Parameters
- cores: Number of cores to use
- samples: Number of samples to generate 
- path: Path of data directory
- experiment_id: Id of experiment, geometry settings defined in gendata/Setup_parallel.py or gendata/Setup_fanflat.py 
- geometry: parallel or fan
- save_all: optional, when set additional files are generated in experiment#[experiment_id]/phantoms to save movement parameters and phantoms at each position. Only recommended for small number of samples.

#### Results
This script generates 3 files: 
- experiment#[experiment_id]/phantoms/phantoms.h5
- experiment#[experiment_id]/sinograms/sinograms.h5
- experiment#[experiment_id]/global_inexactness/y.h5


### generateSinoFromPhantoms
#### Requirements
The following files need to be available in the directory of the new dataset:
- experiment#[experiment_id]/perturbations/phantom_perturbations_[idx].json
- experiment#[experiment_id]/phantoms/phantoms.h5
#### Parameters
- cores: Number of cores to use
- samples: Number of samples to generate 
- path: Path of data directory
- experiment_id: Id of experiment, geometry settings defined in gendata/Setup_parallel.py or gendata/Setup_fanflat.py 
- geometry: parallel or fan

#### Results
This script generates 2 files: 
- experiment#[experiment_id]/sinograms/sinograms.h5
- experiment#[experiment_id]/global_inexactness/y.h5

### Structure of data files
All data files have been created using [PyTables](https://www.pytables.org/usersguide/file_format.html) in the HDF5 format.
The base level of each file is root ('/') followed by the choice of id/data. Depending on the dataset the files also contain:
- phantoms
  - phantoms/original
- sinograms
  - sinograms/original
  - sinograms/perturbed
- eta
  - eta/data 
  - eta_beta/data
  - eta_beta_detector/data

Example structure for eta and sinogram files:
  
#### Eta
![Eta](img/eta_h5.png)
#### Sinogram
![Sinogram](img/sinogram_h5.png)

## Authors and acknowledgment
The code in src/raytrafo has mostly been developed by Johannes Leuschner from University of Bremen.
Philip Oberacker has contributed to PhantomClass.py and phantom.py.
Alice Oberacker is the owner of this repository and has written everything else.
For questions please contact [Alice Oberacker](mailto:alice.oberacker@num.uni-sb.de).

## References
<a id="1">[1]</a> 
Blanke, S.E., Hahn, B.N. and Wald, A., 2020. Inverse problems with inexact forward operator: iterative regularization and application in dynamic imaging. Inverse Problems, 36(12), p.124001.

<a id="2">[2]</a> 
Lütjen, T., Schönfeld, F., Oberacker, A., Leuschner, J., Schmidt, M., Wald, A. and Kluth, T., 2023. Learning-based approaches for reconstructions with inexact operators in nanoCT applications. arXiv preprint arXiv:2307.10474.
https://arxiv.org/abs/2307.10474