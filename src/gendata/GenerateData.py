import sys
import os
import logging

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from PhantomClass import Phantom
from PerturbedPhantom import PerturbedPhantom


class GenerateData:
    """
    GenerateData is a class to create phantoms and sequences of perturbed phantoms.
    """

    def __init__(self, resolution=200, id_sample=None, utils=None, phantom_settings=None, movement_settings=None):
        """

        :param resolution: int, Resolution of field of view
        :param id_sample: int, Id to give to the generated sample
        :param utils: Utils object
        :param phantom_settings: dictionary of phantom settings (see Setup class)
        """
        # Resolution
        self.resolution = resolution
        # Calculate image size
        self.fov_size = 2 * resolution + 1
        # Calculate pixel size
        self.pixel_size = 2 / (2 * resolution + 1)
        # id of sample
        self.sample_id = id_sample
        # phantom settings
        self.phantom_settings = phantom_settings
        # movement settings
        self.movement = movement_settings
        self.Utils = utils
        self.phantom_file_path = os.path.join(self.Utils.phantom_folder, 'phantom_db.h5')

    @staticmethod
    def start_logger_if_necessary():
        logger = logging.getLogger("mylogger")
        if len(logger.handlers) == 0:
            logger.setLevel(logging.INFO)
            sh = logging.StreamHandler()
            sh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
            fh = logging.FileHandler('out.log', mode='w')
            fh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
            logger.addHandler(sh)
            logger.addHandler(fh)
        return logger

    # def load_single_phantom(self):
    #     # check if there is already a phantom in phantoms_db
    #     # if so, load it and it's perturbtion
    #     # for movement data no further phantoms are needed
    #     phantom_file = self.Utils.load_pytable(path=self.phantom_file_path)
    #     phantom = None
    #     if len(phantom_file.root.phantoms.original) > 0:
    #         phantom = phantom_file.root.phantoms.original[0]
    #     phantom_file.close()
    #     return phantom

    def generate_original_perturbed(self, file_phantom, file_phantom_perturbed, lock, save_all=False):
        """
        Generate phantom and sequence of rotated and shifted perturbed phantom.
        :param file_phantom: file name of the non-perturbed sequence
        :param file_phantom_perturbed: file name of the perturbed sequence
        :param lock: multiprocessing lock
        :param save_all: boolean if images and pickles should be saved
        :return: Image of original phantom and sequences of perturbed phantom
        """
        # create a phantom
        phantom = self.generate_phantom()

        # Create sequence of perturbed phantoms
        perturbed_phantoms, perturbation_params = self.generate_perturbed_phantom(phantom)

        # save phantom, perturbed phantom and perturbation params
        self.save_phantoms(phantom, perturbed_phantoms, perturbation_params,
                           lock=lock,
                           file_phantom=file_phantom,
                           file_phantom_perturbed=file_phantom_perturbed,
                           save_all=save_all)

        return phantom, perturbed_phantoms

    def save_phantoms(self, phantom, perturbed_phantoms, perturbation_params, lock=None, file_phantom=None, file_phantom_perturbed=None, save_all=False):

        # save phantom and perturbation params
        self.Utils.save_phantom_to_h5(self.sample_id, phantom, lock)
        self.Utils.save_phantom_perturbations_to_file(self.sample_id, perturbation_params)

        if save_all:
            self.Utils.save_to_pickle(perturbed_phantoms, file_name=file_phantom_perturbed, obj_type='phantom')
            # self.Utils.save_image(phantom, file_phantom, obj_type='phantom')
            # self.Utils.plot_translation(self.random_rotations, self.random_translations, file_name=file_phantom_perturbed)
            self.Utils.save_to_pickle(self.random_translations, file_name='translations_' + file_phantom_perturbed,
                                      obj_type='phantom')
            self.Utils.save_to_pickle(self.random_rotations, file_name='rotations_' + file_phantom_perturbed,
                                      obj_type='phantom')
            # self.Utils.create_video(perturbed_phantoms, file_name='translations_' + file_phantom_perturbed)
        return perturbed_phantoms

    def generate_phantom(self):
        """
        Generate a Phantom
        :return: 2D numpy array
        """
        phantom_generator = Phantom(resolution=self.resolution, phantom_settings=self.phantom_settings)

        phantom = phantom_generator.create_phantom()
        return phantom

    def generate_perturbed_phantom(self, phantom):
        """
        Generate a series perturbed phantoms from single phantom.
        if ``save_all`` save plot of translations and rotations, as well as video or perturbed phantom.
        :param phantom: 2d array
        :param file_name: file name to save to
        :param save_all: whether or not to save image
        :return:
        """
        perturbed = PerturbedPhantom(phantom,
                                     resolution=self.resolution,
                                     movement=self.movement
                                     )
        perturbed_phantoms, perturbation_params = perturbed.create_sequence(normalize=True, norm=1, return_params=True)
        self.random_translations = perturbed.movement.random_transls
        self.random_rotations = perturbed.movement.random_rotations

        return perturbed_phantoms, perturbation_params

    # # archived
    # def generate_phantom_sequences_samples(self, path_phantom, path_phantom_perturbed, save_all=False):
    #     """
    #     Generate num_samples many pairs of rotated, non-perturbed and rotated perturbed videos.
    #     :return: num_samples many pairs of rotated, non-perturbed and rotated perturbed videos
    #     """
    #     # Initialize a list for the pairs of sequences
    #     result = []
    #     original_phantom, sequence_perturbed = None, None
    #     # Iterate over the number of pairs of sequences that are generated
    #     for ii in range(self.number_samples):
    #         # Generate a single pair of sequences
    #         (original_phantom, sequence_perturbed) \
    #             = self.generate_original_perturbed(path_phantom + str(ii), path_phantom_perturbed + str(ii),
    #                                                save_all=save_all)
    #     # Append the pair of sequences
    #     result.append((original_phantom, sequence_perturbed))
    #
    #     return result
