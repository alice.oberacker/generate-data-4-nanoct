import numpy


class SetupFanFlatASTRA:

    def __init__(self, experiment_id='7', save_all=False):

        #####################
        # Folder SETUP ##
        #####################
        self.EXPERIMENT = 'experiment#' + experiment_id
        self.SAVE_ALL = save_all

        ##################
        # Phantom SETUP ##
        ##################
        # more parameters for phantom and features
        self.PH_NAME = "original_phantom_"
        self.PH_PERTURBED_NAME = "sequence_perturbed_"
        self.SINO_NAME = "original_sinogram_"
        self.SINO_PERTURBED_NAME = "sinogram_perturbed_"
        self.PHANTOM_SETTINGS = {#'number_features': self.NUMBER_FEATURES,
                                 'center_area_ph': 0.1,
                                 'center_area_ft': 0.3,
                                 'size_range_ph': (0.4, 0.8),
                                 'size_range_ft': (0.1, 0.2),
                                 'shapes_ph': {"rectangle": 0.3, "ellipse": 0.7},
                                 'shapes_ft': {"rectangle": 0.3, "ellipse": 0.7},
                                 'graylevels_ph': (0.2, 0.8),
                                 'graylevels_ft': (0.2, 0.8)
                                 }
        #####################
        # Experiment SETUP ##
        #####################
        if self.EXPERIMENT == 'experiment#1':
            # Batch settings
            self.BATCH_SIZE = 4
            self.IN_BATCHES = False
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# betas) K:
            self.K = 133
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 361
            # distance source-object d_so (source_distance): 1939 µm
            self.D_so = 60.96798573975044
            self.D_sd = 13443.079261063136
            # angles scanner positions
            beta_step = 360/self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * beta_step, 360 - 0.5 * beta_step, self.K)

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#5':
            # Batch settings
            self.BATCH_SIZE = 4
            self.IN_BATCHES = False
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# betas) K:
            self.K = 133
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 361
            # distance source-object d_so (source_distance): 1939 µm
            self.D_so = 60.96798573975044
            self.D_sd = 13443.079261063136
            # angles scanner positions
            beta_step = 360 / self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * beta_step, 360 - 0.5 * beta_step, self.K)

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'opposite_signs': True,
                'random': {
                    'rotation': 0.001 * self.R,  # scale in truncnorm
                    'shear': 0 * self.R,
                    'translation': (0.001 * self.R, 0.001 * self.R)  # scale in truncnorm
                },
                'worst': {

                },
            }

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#11':
            # Batch settings
            self.BATCH_SIZE = 4
            self.IN_BATCHES = False
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# betas) K:
            self.K = 133
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 361
            # distance source-object d_so (source_distance): 1939 µm
            self.D_so = 60.96798573975044
            self.D_sd = 13443.079261063136
            # angles scanner positions
            beta_step = 360 / self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * beta_step, 360 - 0.5 * beta_step, self.K)

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'opposite_signs': True,
                'random': {
                    'rotation': 0.001 * self.R,  # scale in truncnorm
                    'shear': 0 * self.R,
                    'translation': (0.001 * self.R, 0.001 * self.R)  # scale in truncnorm
                },
                'worst': {

                },
            }

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#2':
            # Batch settings
            self.BATCH_SIZE = 4
            self.IN_BATCHES = False
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 25
            # Number of scanner positions (# betas) K:
            self.K = 111
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 36
            # distance source-object d_so (source_distance): 1939 µm
            self.D_so = 1939 * 2/((322/1281)*self.R)
            self.D_sd = 427538 * 2/((322/1281)*self.R)
            # angles scanner positions
            beta_step = 360/self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * beta_step, 360 - 0.5 * beta_step, self.K)
            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#3':
            # Batch settings
            self.BATCH_SIZE = 4
            self.IN_BATCHES = False
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# betas) K:
            self.K = 333
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 361
            # distance source-object d_so (source_distance): 1939 µm
            self.D_so = 60.96798573975044
            self.D_sd = 13443.079261063136
            # angles scanner positions
            beta_step = 360/self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * beta_step, 360 - 0.5 * beta_step, self.K)

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # number of features
            self.NUMBER_FEATURES = 3
        elif self.EXPERIMENT == 'experiment#4':
            # Batch settings
            self.BATCH_SIZE = 4
            self.IN_BATCHES = False
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# betas) K:
            self.K = 900
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 361
            # distance source-object d_so (source_distance): 1939 µm
            self.D_so = 60.96798573975044
            self.D_sd = 13443.079261063136
            # angles scanner positions
            beta_step = 360/self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * beta_step, 360 - 0.5 * beta_step, self.K)

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # number of features
            self.NUMBER_FEATURES = 3

        else:
            print(f'{self.EXPERIMENT} not found')
            exit(1)

        self.PHANTOM_SETTINGS['number_features'] = self.NUMBER_FEATURES
