import sys
import os
import numpy

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from phantom import get_phantom_feature


class Phantom:

    def __init__(self, resolution, phantom_settings):
        """
        This class can generate a phantom with `number_features` many random features within the basic phantom.
        Currently shapes can be either rectangles or ellipses.

        :param resolution: Resolution of field of view
        :param phantom_settings: Dictionary with all phantom parameters:
            : number_features: Number of features within basic phantom
            : center_area_ph: how far to spread around the center of the image (float between 0 and 1)
            : center_area_ft: how far to spread around the center of the image (float between 0 and 1)
            : size_range_ph: minimum and maximum size of phantom (float between 0 and 1)
            : size_range_ft: minimum and maximum size of feature (float between 0 and 1)
            : shapes_ph: shape of phantom (rectangle, ellipse)
            : shapes_ft: shape of feature (rectangle, ellipse)
            : graylevels_ph: lower and upper bound for graylevel (between 0 and 1)
            : graylevels_ft: lower and upper bound for graylevel (between 0 and 1)
        """
        # Resolution
        self.resolution = resolution
        # Calculate image size
        self.fov_size = 2 * resolution + 1
        # Calculate pixel size
        self.pixel_size = 2 / (2 * resolution + 1)
        # number of features
        self.number_features = phantom_settings['number_features']

        # more parameters for phantom and features
        self.center_area_ph = phantom_settings['center_area_ph']
        self.center_area_ft = phantom_settings['center_area_ft']
        self.size_range_ph = phantom_settings['size_range_ph']
        self.size_range_ft = phantom_settings['size_range_ft']
        self.shapes_ph = phantom_settings['shapes_ph']
        self.shapes_ft = phantom_settings['shapes_ft']
        self.graylevels_ph = phantom_settings['graylevels_ph']
        self.graylevels_ft = phantom_settings['graylevels_ft']

    @staticmethod
    def get_random_configuration(center_area=0.1, size_range=(0, 1), shapes={"rectangle": 0.3, "ellipse": 0.7},
                                 graylevels=(0.2, 0.8)):
        """
        Get a random configuration of center position, size, and shape to create a feature for the phantom
        :param center_area: how far to spread around the center of the image (float between 0 and 1)
        :param size_range: minimum and maximum size of feature (float between 0 and 1)
        :param shapes: shape of feature (rectangle, ellipse)
        :param graylevels: lower and upper bound for graylevel (between 0 and 1)
        :return: position information, size information, rotation angle, shape, and graylevel
        """
        # center of the phantom feature
        x_center = numpy.random.uniform(-center_area, center_area)
        y_center = numpy.random.uniform(-center_area, center_area)

        # size of the phantom feature
        x_size = numpy.random.uniform(size_range[0], size_range[1])
        y_size = numpy.random.uniform(size_range[0], size_range[1])

        # rotation of phantom feature
        rotation_angle = numpy.random.uniform(0, 180)

        # Shape of object: it is more likely to be an ellipse than a rectangle
        s = list(shapes.keys())
        p = list(shapes.values())
        shape = numpy.random.choice(s, p=p)

        # grey level of rectangle
        graylevel = numpy.random.uniform(graylevels[0], graylevels[1])

        return (x_center, y_center), (x_size, y_size), rotation_angle, shape, graylevel

    def create_feature_objects(self):
        """
        Generate number_features many feature objects. The densities are not added but only inserted in empty areas.
        :return: numpy.array with feature objects.
        """
        # Initial filter with only ones (all pixels allowed)
        feature_filter = numpy.ones((self.fov_size, self.fov_size))

        # Initial collection of feature objects (only zeros->no objects)
        feature_objects = numpy.zeros((self.fov_size, self.fov_size))

        # iterate over the number of features that will be added
        for ii in range(self.number_features):
            # get random configuration
            center, size, rotation_angle, shape, graylevel = self.get_random_configuration(
                center_area=self.center_area_ft,
                size_range=self.size_range_ft,
                shapes=self.shapes_ft,
                graylevels=self.graylevels_ft)

            # create a phantom feature according to that configuration
            phantom_feature = get_phantom_feature(self.fov_size, self.pixel_size, center[0], center[1], size[0],
                                                  size[1], rotation_angle, shape, graylevel)

            # add the current phantom_feature to the feature object in the pixels, that are not already taken (where
            # filter is 1)
            feature_objects += numpy.multiply(feature_filter, phantom_feature)

            # get new filter based on new feature_objects.
            # (False * -1) + 1 = 1
            # (True * -1) + 1 = 0
            # So this new filter is 0 precisely in these pixels which are already taken.
            feature_filter = ((feature_objects > 0) * -1) + 1

        return feature_objects

    @staticmethod
    def merge_features_phantom(basic_phantom, feature_objects):
        """
        Merge the feature objects into the basic phantom. The density of the features is NOT added to the basic_phantom's
        density, but the basic_phantom's pixels are set to zero where feature_objects pixels are inserted.
        :param basic_phantom: The basic phantom (numpy array)
        :param feature_objects:  The feature objects (numpy array)
        :return: Merges basic phantom and feature objects (numpy array)
        """
        # Phantom filter (insertion of features is only allowed where pixels are taken by phantom, i.e. >0)
        phantom_filter = (basic_phantom > 0)

        # clear the phantom where we want to insert phantom features
        phantom = numpy.multiply(basic_phantom, ((feature_objects > 0) * -1 + 1))

        # insert feature objects on those pixels where we already have phantom pixels
        phantom = phantom + numpy.multiply(phantom_filter, feature_objects)

        return phantom

    def create_basic_phantom(self):
        """
        Create the basic phantom without any features inside
        :return: Basic phantom (Numpy array)
        """
        # Get random configuration for basic phantom
        center, size, rotation_angle, shape, graylevel \
            = self.get_random_configuration(center_area=self.center_area_ph, size_range=self.size_range_ph,
                                            shapes=self.shapes_ph, graylevels=self.graylevels_ph)
        # Generate basic_phantom
        basic_phantom = get_phantom_feature(self.fov_size, self.pixel_size, center[0], center[1], size[0], size[1],
                                            rotation_angle, shape, graylevel)
        return basic_phantom

    def create_phantom(self):
        """
        Get the complete phantom with random features.
        :return: Phantom (Numpy array)
        """
        # Generate basic_phantom
        basic_phantom = self.create_basic_phantom()
        # get phantom features
        feature_objects = self.create_feature_objects()

        # Merge feature_objects in basic_phantom
        phantom = self.merge_features_phantom(basic_phantom, feature_objects)

        return phantom

