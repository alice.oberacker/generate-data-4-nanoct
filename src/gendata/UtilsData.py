
import os, sys
import pickle
import json
import numpy
import tables
from matplotlib import pyplot as plt
from pathlib import Path

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from Utils import Utils as U


class Utils(U):

    def __init__(self, path, experiment):
        super().__init__(path, experiment)
        self.other_folder = os.path.join(path, experiment)

        self.sinogram_img_folder = os.path.join(path, experiment, 'img', 'sinograms')
        self.phantom_img_folder = os.path.join(path, experiment, 'img', 'phantoms')
        self.other_img_folder = os.path.join(path, experiment, 'img')
        self.create_folders()

    def create_folders(self):
        """
        Create all folders for this experiment. If they already exist, ignore errors.
        If parent folders don't exist, create them.
        :return:
        """
        Path(self.sinogram_folder).mkdir(parents=True, exist_ok=True)
        Path(self.phantom_folder).mkdir(parents=True, exist_ok=True)
        Path(self.weight_folder).mkdir(parents=True, exist_ok=True)
        Path(self.eta_folder).mkdir(parents=True, exist_ok=True)
        Path(self.perturbations_folder).mkdir(parents=True, exist_ok=True)
        Path(self.sinogram_img_folder).mkdir(parents=True, exist_ok=True)
        Path(self.phantom_img_folder).mkdir(parents=True, exist_ok=True)

    def create_dbs(self, nb_detectors, nb_angles, fov_size):

        self.create_db__(file_path=os.path.join(self.sinogram_folder, 'sinogram_db.h5'),
                        dim=(nb_detectors, nb_angles), obj_type='sinogram')
        self.create_db__(file_path=os.path.join(self.phantom_folder, 'phantom_db.h5'),
                        dim=(nb_angles, fov_size, fov_size), obj_type='phantom')
        self.create_db__(file_path=os.path.join(self.eta_folder, 'y.h5'),
                        dim=(nb_detectors, nb_angles), obj_type='eta')

    def create_db__(self, file_path, dim, obj_type):
        """
        Create h5 files with pytables for phantoms, and sinograms in case they don't exist yet.
        """
        if not os.path.isfile(file_path):
            # file does not exist yet, so create it
            filter = tables.Filters(complib='zlib', complevel=5)
            f = tables.open_file(file_path, mode='w', filters=filter)
            if obj_type == 'eta':
                f = self.__create_eta_tables(f, dim)
            elif obj_type == 'phantom':
                f = self.__create_phantom_tables(f, dim)
            elif obj_type == 'sinogram':
                f = self.__create_sinogram_tables(f, dim)
            f.close()

    @staticmethod
    def __create_sinogram_tables(file_obj, dim):
        atom = tables.Float64Atom()
        atom_id = tables.Int64Col()

        file_obj.create_group("/", 'sinograms', 'original and perturbed sinograms')
        file_obj.create_group("/", 'id', 'sample ids')

        file_obj.create_earray(file_obj.root.sinograms, 'original', atom, (0, *dim))
        file_obj.create_earray(file_obj.root.sinograms, 'perturbed', atom, (0, *dim))
        file_obj.create_earray(file_obj.root.id, 'data', atom_id, (0,))
        return file_obj

    @staticmethod
    def __create_phantom_tables(file_obj, dim):
        atom = tables.Float64Atom()
        atom_id = tables.Int64Col()

        file_obj.create_group("/", 'phantoms', 'original phantoms')
        file_obj.create_group("/", 'id', 'sample ids')

        file_obj.create_earray(file_obj.root.phantoms, 'original', atom, (0, *dim[1:]))
        file_obj.create_earray(file_obj.root.phantoms, 'perturbed', atom, (0, *dim))
        file_obj.create_earray(file_obj.root.id, 'data', atom_id, (0,))
        return file_obj

    @staticmethod
    def __create_eta_tables(file_obj, dim):
        atom = tables.Float64Atom()
        atom_id = tables.Int64Col()

        file_obj.create_group("/", 'eta_beta_detector', '2-d etas')
        file_obj.create_group("/", 'eta_beta', '1-d etas')
        file_obj.create_group("/", 'eta', '0-d etas')
        file_obj.create_group("/", 'id', 'sample ids')

        file_obj.create_earray(file_obj.root.eta_beta_detector, 'data', atom, (0, *dim))
        file_obj.create_earray(file_obj.root.eta_beta, 'data', atom, (0, dim[1]))
        file_obj.create_earray(file_obj.root.eta, 'data', atom, (0,))
        file_obj.create_earray(file_obj.root.id, 'data', atom_id, (0,))
        return file_obj

    def save_to_pickle(self, obj, file_name='file', obj_type='phantom'):
        """
        Saves object to pickle file. Depending on the object type it will be written to a specific folder.
        :param obj:
        :param file_name:
        :param obj_type: phantom | sinogram | weights
        :return:
        """
        if obj_type == 'phantom':
            path = os.path.join(self.phantom_folder, file_name + ".pkl")
        elif obj_type == 'sinogram':
            path = os.path.join(self.sinogram_folder, file_name + ".pkl")
        elif obj_type == 'weights':
            path = os.path.join(self.weight_folder, file_name + ".pkl")
        else:
            path = os.path.join(self.other_folder, file_name + ".pkl")

        pickle.dump(obj, open(path, 'wb'))

    def save_phantom_to_h5(self, sample_id, original=None, lock=None, perturbed=None):
        """
        Write the phantom with its corresponding sample id to file.
        :param sample_id:
        :param original:
        :param lock: Multiprocessing lock
        :return:
        """
        lock.acquire()
        path = os.path.join(self.phantom_folder, 'phantom_db.h5')
        h5_obj = self.load_pytable(path, mode='a')
        # update file with new original
        if original is not None:
            dim = original.shape
            h5_obj.root.phantoms.original.append(original.reshape((1, *dim)))
        h5_obj.root.id.data.append(numpy.array([sample_id]))
        if perturbed is not None:
            dim = perturbed.shape
            h5_obj.root.phantoms.perturbed.append(perturbed.reshape((1, *dim)))
        h5_obj.close()
        lock.release()

    def save_sinogram_to_h5(self, sample_id, values, lock):
        """
        Write the phantom with its corresponding sample id to file.
        :param sample_id:
        :param values:
        :param lock: Multiprocessing lock
        :return:
        """
        lock.acquire()
        path = os.path.join(self.sinogram_folder, 'sinogram_db.h5')
        dim = values[0].shape
        h5_obj = self.load_pytable(path, mode='a')
        # update file with new original
        h5_obj.root.sinograms.original.append(values[0].reshape((1, *dim)))
        h5_obj.root.sinograms.perturbed.append(values[1].reshape((1, *dim)))
        h5_obj.root.id.data.append(numpy.array([sample_id]))
        h5_obj.close()
        lock.release()

    def save_norm_to_file(self, sample_id, value, lock, dim):
        """
        Write the norm original with its corresponding sample id to file.
        { id: { 'eta': float, 'eta_beta': list(float) }, ... }
        :param sample_id:
        :param value:
        :param lock: Multiprocessing lock
        :param dim: dimensions of etas
        :return:
        """
        lock.acquire()
        path = os.path.join(self.eta_folder, 'y.h5')
        h5_obj = self.load_pytable(path, mode='a')
        # update file with new original
        # h5_obj.root.eta_beta_detector.data.append(original['eta_beta_detector'].reshape((1, *dim)))
        h5_obj.root.eta_beta.data.append(value['eta_beta'].reshape((1, dim[1])))
        h5_obj.root.eta.data.append(numpy.array([value['eta']]))
        h5_obj.root.id.data.append(numpy.array([sample_id]))
        h5_obj.close()
        lock.release()

    def save_phantom_perturbations_to_file(self, sample_id, perturbation_params):
        """
        Write the phantom perturbations for a sample to file.
        :param sample_id:
        :param perturbation_params:
        :return:
        """
        file_name = 'phantom_perturbations' + ('' if sample_id is None else f'_{sample_id}') + '.json'
        path = os.path.join(self.perturbations_folder, file_name)
        with open(path, 'w') as fd:
            # convert list of lists to str to avoid indentation
            json.dump([{'affine_transform': str(p.tolist())} for p in perturbation_params], fd, indent=1)

    @staticmethod
    def calc_norm(x, y, axis=None):
        """
        Calculate the L2 norm (Frobenius norm) of the x - y
        :param x:
        :param y:
        :return:
        """
        if axis == -1:
            return abs(x-y)
        else:
            # return norm(x - y, axis=axis)
            return abs(x - y).max(axis=axis)

    def calc_errors(self, x, y, types=['eta', 'eta_beta']):
        result = {}
        if 'eta' in types:
            y_ = self.calc_norm(x, y)
            result['eta'] = y_
        if 'eta_beta' in types:
            y_beta = self.calc_norm(x, y, axis=0)
            result['eta_beta'] = y_beta
        if 'eta_beta_detector' in types:
            y_beta_detector = self.calc_norm(x, y, axis=-1)
            result['eta_beta_detector'] = y_beta_detector
        return result

    def get_max_sample_id(self):
        """
        Get maximum id of already created phantoms. Ids and error values are stored in "global model inexactness/y.csv".
        :return:
        """
        path = os.path.join(self.eta_folder, 'y.h5')
        obj = self.load_pytable(path, mode='r')
        if len(obj.root.id.data) > 0:
            # obj is not empty
            # + 1 because we start counting at 0
            max_id = max(obj.root.id.data) + 1
        else:
            # j_obj is empty
            max_id = 0
        obj.close()
        return max_id

    def plot_translation(self, rotation, translation, sinus_noise=True, file_name=''):
        """
        Plot the x, y, z shifts of perturbed sinogram
        :param rotation: List of rotations
        :param translation: List of x-y translations
        :param sinus_noise: True if noise is based on sinus waves.
        :param file_name: Name of file to store the plot in
        :return:
        """
        xy = list(zip(*translation))
        plt.plot(xy[0])
        plt.plot(xy[1])
        plt.plot(rotation)
        plt.legend(['x', 'y', 'rotation'])
        plt.xticks(numpy.arange(0, len(xy[0]), 10));
        if sinus_noise:
            plt.title(f'Sinus noise')
        else:
            plt.title('Random noise')

        plt.savefig(os.path.join(self.phantom_img_folder,
                                 f'{file_name}.png'))
        plt.close()

    @staticmethod
    def chunks(lst, n):
        """
        Generator for successive n-sized chunks from list.
        :param lst: list to group into batches
        :param n: Number of items in batch
        :return: Subset of list
        """
        for i in range(0, len(lst), n):
            yield lst[i:i + n]
