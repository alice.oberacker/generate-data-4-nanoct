import os, sys
from numpy import identity as np_identity
import numpy
from random import uniform, randint
from random import choice
from scipy.stats import truncnorm
import logging
from matplotlib import pyplot as plt

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)


class Movement:
    """
    Base class for movement generation. creates rotations, shears and x, y translations.
    Sinus waves, random and linear movement are currently implemented.
    """
    def __init__(self, movement):
        self.number_transforms = movement['number_transforms']
        self.max_shift = movement['total_max_shift']
        self.movement = movement

        self.sinus_movement = None
        self.linear_movement = None
        self.random_movement = None
        self.random_rotations = numpy.zeros(self.number_transforms)
        self.random_shears = numpy.zeros(self.number_transforms)
        self.random_transls = numpy.zeros(self.number_transforms)

    def generate_movement(self):
        """
        Generates a list of movement patterns, which can be added together.
        i.e. linear + sinus
        aka PerturbedPhantom.get_random_vals
        :return:
        """
        movements = []
        if 'sinus' in self.movement.keys():
            self.sinus_movement = SinusMovement(self.movement)
            movements.append(self.sinus_movement)
        if 'linear' in self.movement.keys():
            self.linear_movement = LinearMovement(self.movement)
            movements.append(self.linear_movement)
        if 'random' in self.movement.keys():
            self.random_movement = RandomMovement(self.movement)
            movements.append(self.random_movement)

        random_transls_xs = numpy.zeros(self.number_transforms)
        random_transls_ys = numpy.zeros(self.number_transforms)
        for m in movements:
            m.generate_movement()
            self.random_shears += m.random_shears
            self.random_rotations += m.random_rotations
            random_transls_xs += m.random_transl_xs
            random_transls_ys += m.random_transl_ys
        self.random_transls = list(zip(random_transls_xs, random_transls_ys))


class LinearMovement(Movement):
    """ Linear Movement """
    def __init__(self, movement):
        """
        Movement is defined by dictionary:
        'linear': {
                    'rotation': _,
                    'shear': _,
                    'translation': (_, _)
                }
        each value defines the range of possible values for the movement type.
        i.e. rotation: 0.01 -> value from [-0.01, 0.01]
        :param movement: dict
        """
        super().__init__(movement)
        assert 'linear' in movement.keys(), 'wrong movement key'
        self.translation_x = truncnorm(a=-1, b=1, scale=movement['linear']['translation'][0]).rvs(size=1)
        self.translation_y = truncnorm(a=-1, b=1, scale=movement['linear']['translation'][1]).rvs(size=1)
        self.rotation = truncnorm(a=-1, b=1, scale=movement['linear']['rotation']).rvs(size=1)
        self.shear = truncnorm(a=-1, b=1, scale=movement['linear']['shear']).rvs(size=1)

        self.random_rotations = numpy.zeros(self.number_transforms)
        self.random_shears = numpy.zeros(self.number_transforms)
        self.random_transl_xs = numpy.zeros(self.number_transforms)
        self.random_transl_ys = numpy.zeros(self.number_transforms)

    def generate_movement(self):
        self.random_shears = numpy.cumsum([self.shear] * self.number_transforms)
        self.random_rotations = numpy.cumsum([self.rotation] * self.number_transforms)
        self.random_transl_xs = numpy.cumsum([self.translation_x] * self.number_transforms)
        self.random_transl_ys = numpy.cumsum([self.translation_y] * self.number_transforms)


class RandomMovement(Movement):

    def __init__(self, movement):
        """
        Movement is defined by dictionary:
        'random': {
                    'rotation': _,
                    'shear': _,
                    'translation': (_, _)
                }
        each value defines the range of possible values for the movement type.
        i.e. rotation: 0.01 -> value from [-0.01, 0.01]
        :param movement: dict
        """
        super().__init__(movement)
        if 'random' in movement.keys():
            self.translation_x = movement['random']['translation'][0]
            self.translation_y = movement['random']['translation'][1]
            self.rotation = movement['random']['rotation']
            self.shear = movement['random']['shear']

        self.random_rotations = numpy.zeros(self.number_transforms)
        self.random_shears = numpy.zeros(self.number_transforms)
        self.random_transl_xs = numpy.zeros(self.number_transforms)
        self.random_transl_ys = numpy.zeros(self.number_transforms)

    def generate_movement(self):
        # get random shears
        self.random_shears = truncnorm(a=-1, b=1, scale=self.shear).rvs(size=self.number_transforms)
        # get random rotations
        self.random_rotations = truncnorm(a=-1, b=1, scale=self.rotation).rvs(size=self.number_transforms)
        # get random translations x direction
        self.random_transl_xs = truncnorm(a=-1, b=1, scale=self.translation_x).rvs(size=self.number_transforms)
        # get random translations y direction
        self.random_transl_ys = truncnorm(a=-1, b=1, scale=self.translation_y).rvs(size=self.number_transforms)

        assert len(self.random_transl_xs) == self.number_transforms
        assert len(self.random_transl_ys) == self.number_transforms

        # zip the x and y translations to a list of tuples
        # self.random_transls = list(zip(random_transl_xs, random_transl_ys))
        return


class SinusMovement(Movement):
    """ Sinus Movement """
    def __init__(self, movement):
        """
        Movement is defined by dictionary:
        'sinus': {
                    'number_waves': _,
                    'amplitude': lambda: (yield numpy.random.normal(_,_) * self.R),
                    'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                    'frequency': lambda: (yield numpy.random.uniform(_,_)),
                    'dampening': lambda: (yield numpy.random.uniform(_,_))
                }
        each value defines the range of possible values for the movement type.
        i.e. 'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09))
                -> generator producing values from a Normal distribution
        :param movement: dict
        """
        super().__init__(movement)
        if 'sinus' in movement.keys():
            self.number_waves = movement['sinus']['number_waves']
            self.amplitude = movement['sinus']['amplitude']
            self.amplitude_r = movement['sinus']['amplitude_r']
            self.frequency_gen = movement['sinus']['frequency']
            self.dampening_gen = movement['sinus']['dampening']
            self.amplitude_x = None
            self.amplitude_y = None

        self.random_rotations = numpy.zeros(self.number_transforms)
        self.random_shears = numpy.zeros(self.number_transforms)
        self.random_transl_xs = numpy.zeros(self.number_transforms)
        self.random_transl_ys = numpy.zeros(self.number_transforms)

    def generate_movement(self):
        """
        aka PerturbedPhantom.get_random_vals
        :return:
        """
        # get random sinus waves
        try:
            sinus_transl_xs, sinus_transl_ys, _ = self.gen_sinus_noise(time_steps=self.number_transforms,
                                                                       numb_waves=self.number_waves,
                                                                       max_shift=self.max_shift)
        except AssertionError as e:
            print('Could not scale sinus waves to stay within bounds.')
            # continue only with random noise, by setting sinus noise to 0
            sinus_transl_xs = numpy.zeros((self.number_transforms))
            sinus_transl_ys = numpy.zeros((self.number_transforms))
        assert len(sinus_transl_xs) == self.number_transforms
        assert len(sinus_transl_ys) == self.number_transforms
        self.random_transl_xs = sinus_transl_xs
        self.random_transl_ys = sinus_transl_ys
        return

    @staticmethod
    def damped_sinus(amplitude, frequency, dampening, time):
        """
        Calculate the value of a damped sinus wave at a given point in time.
        The formula of the damped sinus wave is:

        wave(t) = amplitude * sinus(w * t) * exp(-c * t)

        :param amplitude:
        :param frequency:
        :param dampening:
        :param time: point in time
        :return: value at point t
        """
        return amplitude * numpy.sin(frequency * time) * numpy.exp(-dampening * time)

    def sinus_wave(self, start, timesteps=180):
        """
        Calculates a sinus wave for ``timesteps`` many points. x and y shifts happen at different points in time, but
        with the same frequency and dampening.
        :param start: tuple of start points for x and y
        :param amplitude: tuple of amplitude for x and y
        :param freq: float of frequency of wave
        :param damp: float of dampening of wave
        :param timesteps: how many timesteps to calculate
        :return: list of values for x and y
        """
        # calculate length of waves
        wave_duration_x = timesteps - start[0]
        wave_duration_y = timesteps - start[1]
        # generate waves for x and y
        sinus_transl_xs = [self.damped_sinus(self.amplitude_x, self.frequency, self.dampening, t) for t in
                           range(wave_duration_x)]
        sinus_transl_ys = [self.damped_sinus(self.amplitude_y, self.frequency, self.dampening, t) for t in
                           range(wave_duration_y)]
        # add zeros to the start of the lists to shift wave to given start point
        sinus_transl_xs = numpy.concatenate([numpy.zeros(start[0]), sinus_transl_xs])
        sinus_transl_ys = numpy.concatenate([numpy.zeros(start[1]), sinus_transl_ys])

        return sinus_transl_xs, sinus_transl_ys

    @staticmethod
    def random_sinus_direction(xs, ys):
        rnd = choice([1, -1])
        xs = [x * rnd for x in xs]
        rnd = choice([1, -1])
        ys = [y * rnd for y in ys]
        return xs, ys

    @staticmethod
    def smooth(x, box_pts, mode='same'):
        if box_pts == 0:
            box_pts = 1
        box = numpy.ones(box_pts) / box_pts
        x_smooth = numpy.convolve(x, box, mode=mode)
        return x_smooth

    def set_sinus_config(self):
        """
        Generate random x,y amplitude, frequency and dampening.
        :return:
        """
        # amplitudes
        ampl = next(self.amplitude())
        amplitude_r = next(self.amplitude_r())
        self.amplitude_x = amplitude_r * ampl
        self.amplitude_y = numpy.sqrt(ampl ** 2 - self.amplitude_x ** 2)
        # Frequency of sinus wave
        self.frequency = next(self.frequency_gen())
        # sinus dampening
        self.dampening = next(self.dampening_gen())

    def gen_sinus_noise(self, time_steps=180, numb_waves=20, max_shift=16):
        """
        Generate ``numb_waves`` many sinus waves randomly distributed across ``time_steps``.
        Overlapping waves are added up, the maximum shift is defined by ``max_shift``.
        :param time_steps: int, time steps to calculate waves for
        :param numb_waves: int, number of waves to create
        :param max_shift: int, maximum x, y displacement
        :return: list of x and y shifts, dictionary containing all random sinus parameters.
        """
        # generate random start points of waves for x and y
        start_points_x = sorted([randint(1, time_steps) for _ in range(numb_waves)])
        start_points_y = sorted([randint(1, time_steps) for _ in range(numb_waves)])

        xs = ys = numpy.zeros(time_steps)
        rnd_sett = {}
        for i in range(numb_waves):
            # get start point for x and y
            start_x = start_points_x[i]
            start_y = start_points_y[i]

            # generate new random vars for sinus wave
            self.set_sinus_config()

            # generate sinus wave
            sinus_transl_xs, sinus_transl_ys = self.sinus_wave((start_x, start_y),
                                                               time_steps)
            # randonly change direction of sinus wave
            sinus_transl_xs, sinus_transl_ys = self.random_sinus_direction(sinus_transl_xs, sinus_transl_ys)

            # filter, in case the addition of sinus waves exceeds the maximum amplitude
            xs_new = numpy.add(xs, sinus_transl_xs)
            ys_new = numpy.add(ys, sinus_transl_ys)
            xs_max = max(map(abs, xs_new))
            ys_max = max(map(abs, ys_new))
            # in case xs amplitude is too big, scale the new wave down
            mult = 0
            if xs_max > max_shift:
                # scaling such that xs + sinus_transl_xs <= max_shift
                mult = (max_shift - max(map(abs, xs))) / max(map(abs, sinus_transl_xs)) * 0.9
                sinus_transl_xs = [x * mult for x in sinus_transl_xs]
                xs_new = numpy.add(xs, sinus_transl_xs)
                if max(map(abs, xs_new)) > max_shift:
                    print('fail')
                assert max(map(abs, xs_new)) <= max_shift
            # in case ys amplitude is too big, scale the new wave down
            if ys_max > max_shift:
                # scaling such that ys + mult * sinus_transl_ys < max_shift
                mult = (max_shift - max(map(abs, ys))) / max(map(abs, sinus_transl_ys)) * 0.9
                sinus_transl_ys = [y * mult for y in sinus_transl_ys]
                ys_new = numpy.add(ys, sinus_transl_ys)
                if max(map(abs, ys_new)) > max_shift:
                    print('fail')
                assert max(map(abs, ys_new)) <= max_shift
            # self.plot_wave(sinus_transl_xs, sinus_transl_ys, xs_new, ys_new, xs, ys)

            # add new wave up on existing ones
            xs = self.smooth(xs_new, int(len(xs_new) * 0.015))
            ys = self.smooth(ys_new, int(len(ys_new) * 0.015))
            # save random sinus settings to list
            rnd_sett[i] = {'start_x': start_x, 'start_y': start_y,
                           'amplitude_x': self.amplitude_x, 'amplitude_y': self.amplitude_y,
                           'freq': self.frequency, 'dampening': self.dampening}

        return xs, ys, rnd_sett

    @staticmethod
    def plot_wave(new_wave1, new_wave2, wave_total1, wave_total2, wave_before1, wave_before2):
        fig, ax = plt.subplots(1, 3, figsize=(18, 4))
        ax[0].plot(new_wave1)
        ax[0].plot(new_wave2)
        ax[1].plot(wave_total1)
        ax[1].plot(wave_total2)
        ax[2].plot(wave_before1)
        ax[2].plot(wave_before2)
        plt.show()