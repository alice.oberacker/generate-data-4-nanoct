from skimage import io, transform
from numpy import linalg as np_linalg
from numpy import array as np_array
from numpy import matmul as np_matmul


class Transformation:

    def __init__(self, resolution, rotation, shear, translations, matrix=None):
        """
        This class performs a single transformation by creating a matrix (numpy array) that rotates the phantom around its axis.

        :param resolution: Resolution of field of view
        :param rotation: strength of rotation
        :param shear: strength of shear
        :param translations: strength of translation x-y direction
        :param matrix: transformation matrix (Numpy array)
        """
        self.fov_size = 2 * resolution + 1
        self.rotation = rotation
        self.shear = shear
        self.translation = translations  # (x,y) translations
        self.matrix = matrix

    def create_transform_origin(self):
        # Create affine transform
        affine_transform = self.create_transform()

        # shift transformation such that center of image is rotation axis
        affine_transform = self.origin_shift(affine_transform)

        # Get matrix (numpy array) from skimage transformation object
        matrix_new = affine_transform.params
        return matrix_new

    def create_transform(self, matrix=None):
        """
        Create a transformation object based on either rotation, shear, and translation or a transformation matrix.
        :param matrix: 3x3 transformation matrix
        :return: skimage transformation object
        """
        # If no matrix is provided use rotation, shear, and translation to create transformation object
        # Note that this transformation rotates around the origin (upper left corner?), not the center of the image.

        if matrix is None:
            affine_transform = transform.AffineTransform(rotation=self.rotation, shear=self.shear,
                                                         translation=self.translation)
        # If matrix is provided use it to create transformation object
        else:
            affine_transform = transform.AffineTransform(matrix)
        return affine_transform

    def origin_shift(self, affine_transform):
        """
        Takes a transform object that rotates around the upper left corner and transforms it into a transformation object
        the rotates around the center of the image.
        :param affine_transform: skimage transformation object
        :return: skimage transformation object
        """
        # Extract numpy array from transformation object
        matrix = affine_transform.params

        # Get x and y width
        x_width = y_width = self.fov_size

        # Create shift and inverse shift matrices
        shift1 = np_array([[1, 0, y_width / 2],
                           [0, 1, x_width / 2],
                           [0, 0, 1]])

        shift2 = np_array([[1, 0, -y_width / 2],
                           [0, 1, -x_width / 2],
                           [0, 0, 1]])

        # Multiply matrices to get a transformation matrix that rotates the image around its center
        new_matrix = shift1 @ matrix @ shift2

        # Create and return new Affine transform
        return transform.AffineTransform(new_matrix)

    @staticmethod
    def norm_matrix(matrix, normalize=True, norm=1):
        """
        Normalize a non-zero matrix to a given norm
        :param matrix: matrix (numpy array), not zero-matrix
        :param normalize: If True, normalize, if False return matrix as is
        :param norm: to which value to normalize (float)
        :return: normalized matrix
        """
        if normalize:
            # Get norm of new transformation matrix
            norm_temp = np_linalg.norm(matrix)

            # Normalize new transformation matrix to old norm
            matrix = matrix / norm_temp * norm
        return matrix

    @staticmethod
    def apply_transform(image, affine_transform, cval=0):
        """
        Apply a skimage transformation object to an image
        :param image: skimage image object (numpy array)
        :param affine_transform: skimage transformation object
        :param cval: how to pad after transformation (0=black, 1=white)
        :return: transformed image
        """
        image = transform.warp(image, inverse_map=affine_transform, cval=cval)
        return image

    @staticmethod
    def get_transformation_mat(matrix_new, matrix):
        # multiply matrices to get next transformation matrix
        matrix = np_matmul(matrix_new, matrix)
        return matrix

    def generate_transformation_matrix(self, matrix, normalize=True, norm=1):
        # Get matrix (numpy array) from skimage transformation object, with center of image as rotation axis
        matrix_new = self.create_transform_origin()

        # multiply matrices to get next transformation matrix
        matrix = np_matmul(matrix_new, matrix)

        # normalize matrix
        matrix = self.norm_matrix(matrix, normalize=normalize, norm=norm)

        # Create skimage transformation object from new transformation matrix
        affine_transform_next = self.create_transform(matrix=matrix)
        return affine_transform_next, matrix
