import argparse
import os
import sys
from datetime import datetime
import numpy
from scipy import sparse

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from UtilsData import Utils
from Setup_parallel import SetupParallel
from Setup_fanflat import SetupFanFlatASTRA
from raytrafo.ParallelASTRA import get_ray_trafo_matrix_parallel_2d_astra
from raytrafo.FanBeamASTRA import get_angles_fan_beam, get_ray_trafo_matrix_fan_beam_astra


def generate_weights(path_str, setup_obj):
    """
    Generate weights in batches of scanner angles or sequential.
    For each experiment we get a weight matrix for each ray.
    Therefore we get (number of rays) x (number of detector angles) many weights.

    :param path_str: Path to data folder
    :param setup_obj: contains experiment parameters
    :return:
    """
    utils = Utils(path_str, setup_obj.EXPERIMENT)

    if setup_obj.DET_POINTS_ON_BOUNDARY:
        det_pixel_size = 2.*numpy.sqrt(2) / (2 * setup_obj.P + 1-1)
    else:
        det_pixel_size = 2.*numpy.sqrt(2) / (2 * setup_obj.P + 1)
    # create sparse ray trafo matrix
    ray_trafo_matrix = get_ray_trafo_matrix_parallel_2d_astra(im_shape=2 * setup_obj.R + 1,
                                                              angles=numpy.radians(setup_obj.ANGLES),
                                                              det_shape=2 * setup_obj.P + 1,
                                                              det_px_size=det_pixel_size,
                                                              flatten=True,
                                                              sparse=True,
                                                              show_pbar=True)

    # format file name: "K_angle_P_detectors_R_res"
    file_name = utils.weight_file.format(setup_obj.K, setup_obj.P, setup_obj.R)

    sparse.save_npz(os.path.join(utils.weight_folder, file_name + '.npz'), ray_trafo_matrix)


def generate_weights_fan(path_str, setup_obj):
    """
    Generate weights in batches of scanner angles or sequential.
    For each experiment we get a weight matrix for each ray.
    Therefore we get (number of rays) x (number of detector angles) many weights.

    :param path_str: Path to data folder
    :param setup_obj: contains experiment parameters
    :return:
    """
    utils = Utils(path_str, setup_obj.EXPERIMENT)

    # create sparse ray trafo matrix
    angles = get_angles_fan_beam(setup_obj.K, use_angles_like_odl=True)
    # get csr matrix
    ray_trafo_matrix = get_ray_trafo_matrix_fan_beam_astra(im_shape=(2 * setup_obj.R + 1, 2 * setup_obj.R + 1),
                                                           src_radius=setup_obj.D_so,
                                                           det_radius=setup_obj.D_sd - setup_obj.D_so,
                                                           angles=angles,
                                                           det_shape=2 * setup_obj.P + 1,
                                                           flatten=True,
                                                           show_pbar=True)

    # format file name: "K_angle_P_detectors_R_res"
    file_name = utils.weight_file.format(setup_obj.K, setup_obj.P, setup_obj.R)

    sparse.save_npz(os.path.join(utils.weight_folder, file_name + '.npz'), ray_trafo_matrix)


def read_args():
    """
    Read execution parameters.

    --path of data directory
    --experiment_id id of experiment to execute
    --geometry geometry of ct scan
    """
    parser = argparse.ArgumentParser(description='Inputs for weight generation.')
    parser.add_argument('--path', '-p', type=str, help='the path to the data directory.')
    parser.add_argument('--experiment_id', '-e', type=str, help='id of experiment to execute.')
    parser.add_argument('--geometry', '-g', type=str, default='parallel', help="'parallel' or 'fan' beam geometry.")

    arguments = parser.parse_args()
    return arguments


if __name__ == '__main__':
    """
    Generate weights for a given parallel geometry.
    Geometry parameters are defined in Setup_parallel.py
    """

    args = read_args()

    start = datetime.now()
    print('input: ' + args.path)
    if args.geometry == 'parallel':
        setup = SetupParallel(experiment_id=args.experiment_id)
        generate_weights(args.path, setup_obj=setup)

    elif args.geometry == 'fan':
        setup = SetupFanFlatASTRA(experiment_id=args.experiment_id)
        generate_weights_fan(args.path, setup_obj=setup)
    else:
        raise ValueError(f"'{args.geometry}' is not a valid geometry.")

    end = datetime.now()

    total = end - start
    print('#' * 20)
    print(f'the program took {total.seconds} seconds to complete.')
    print('#' * 20)
