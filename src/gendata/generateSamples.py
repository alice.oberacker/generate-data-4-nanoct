import argparse
import multiprocessing
import sys
import os
from joblib import Parallel, delayed
import logging
from os.path import abspath
from datetime import datetime
import tables
from tqdm import tqdm

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from GenerateData import GenerateData
from Setup_parallel import SetupParallel
from Setup_fanflat import SetupFanFlatASTRA as SetupFan
from UtilsData import Utils
from Sinogram import Sinogram, SinogramSparse


def start_logger_if_necessary():
    """
    This is a logging function to debug parallelised code.
    :return:
    """
    logger = logging.getLogger("mylogger")
    if len(logger.handlers) == 0:
        logger.setLevel(logging.INFO)
        sh = logging.StreamHandler()
        sh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
        fh = logging.FileHandler('out.log', mode='w')
        fh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
        logger.addHandler(sh)
        logger.addHandler(fh)
    return logger


def generate_samples(sample, path, setup_obj, lock_ph, lock_si, lock_eta):
    """
    Generate Phantoms and Sinograms for original and perturbed cases
    :param sample: the id of the sample
    :param path: Path to the data folder (str)
    :param setup_obj: Setup object of experiment
    :param lock_ph: Multiprocessing lock
    :param lock_si: Multiprocessing lock
    :param lock_eta: Multiprocessing lock
    :return:
    """
    utils = Utils(path, setup_obj.EXPERIMENT)
    # Function that generates a sequence
    try:
        phantom, perturbed_phantoms = generate_phantoms(sample_id=sample, utils_obj=utils, setup_obj=setup_obj, lock=lock_ph)
    except Exception as e:
        print('exception when generating phantoms')
        raise e
    # generate the sinograms for original and perturbed phantoms
    # generate_sinograms(phantom, perturbed_phantoms, utils_obj=utils, setup_obj=setup_obj, sample_id=sample, lock=lock)
    try:
        generate_fp(phantom, perturbed_phantoms,
                    utils_obj=utils, setup_obj=setup_obj, sample_id=sample,
                    lock_si=lock_si, lock_eta=lock_eta)
    except Exception as e:
        print('exception when generating sinograms')
        raise e


def generate_phantoms(sample_id, utils_obj, setup_obj, lock):
    """
    Generate original and perturbed phantoms for ``sample``.
    :param sample_id: id for sample
    :param utils_obj: Instance of Utils class
    :param setup_obj: Instance of Setup class or ParallelSetup class
    :return: 2d array for phantom and 3d array for perturbed phantom
    """
    generator = GenerateData(resolution=setup_obj.R,
                             id_sample=sample_id,
                             utils=utils_obj,
                             phantom_settings=setup_obj.PHANTOM_SETTINGS,
                             movement_settings=setup_obj.MOVEMENT)

    # create phantom
    phantom, perturbed_phantoms = generator.generate_original_perturbed(file_phantom=setup_obj.PH_NAME + str(sample_id),
                                                                        file_phantom_perturbed=setup_obj.PH_PERTURBED_NAME + str(
                                                                            sample_id),
                                                                        save_all=setup_obj.SAVE_ALL,
                                                                        lock=lock)
    return phantom, perturbed_phantoms


def generate_fp(phantom, perturbed_phantoms, utils_obj, setup_obj, sample_id, lock_si, lock_eta):
    """
    Generate the sinograms for ``sample`` and calculate the error between original and perturbed sinogram.
    More efficient than generate_sinogram, due to use of SinogramSparse
    :param phantom: Numpy 2d array
    :param perturbed_phantoms: Numpy 3d array
    :param utils_obj: Instance of Utils class
    :param setup_obj: Instance of Setup class
    :param sample_id: id for sample
    :param lock_si: Multiprocessing lock for sinogram file
    :param lock_eta: Multiprocessing lock for eta file
    :return:
    """
    print(f'generating sinograms {sample_id}')
    # path to weight file(s)
    weight_path = utils_obj.weight_path.format(setup_obj.K, setup_obj.P, setup_obj.R)

    gen_sinogram = SinogramSparse(phantom, perturbed_phantoms, id_sample=sample_id,
                                  weights_path=weight_path, utils=utils_obj,
                                  fov_shape=(2*setup_obj.R+1, 2*setup_obj.R+1),
                                  geometry_shape=(setup_obj.K, 2*setup_obj.P+1))

    gen_sinogram.generate_sinogram(phantom)
    gen_sinogram.generate_perturbed_sinogram(perturbed_phantoms)

    print(f'saving sinograms {sample_id}')
    gen_sinogram.save_sinogram(sample_id, lock_si, save_all=setup_obj.SAVE_ALL,
                               file_sinogram=setup_obj.SINO_NAME + str(sample_id),
                               file_sinogram_perturbed=setup_obj.SINO_PERTURBED_NAME + str(sample_id)
                               )

    # save to pickle and img
    # file_sinogram = setup_obj.SINO_NAME + str(sample_id)
    # file_sinogram_perturbed = setup_obj.SINO_PERTURBED_NAME + str(sample_id)
    # if (phantom is not None) and file_sinogram:
    #     utils_obj.save_to_pickle(sinogram, file_name=file_sinogram, obj_type='sinogram')
    # if (perturbed_phantoms is not None) and file_sinogram_perturbed:
    #     utils_obj.save_to_pickle(perturbed_sinogram, file_name=file_sinogram_perturbed, obj_type='sinogram')

    print(f'calculating eta {sample_id}')
    try:
        gen_sinogram.calculate_error(lock_eta)
    except Exception as e:
        raise e


def generate_sinograms(phantom, perturbed_phantoms, utils_obj, setup_obj, sample_id, lock):
    """
    # method without ODL, less efficient than generate_fp, used for iterative algorithms, to cal
    Generate the sinograms for ``sample`` and calculate the error between original and perturbed sinogram.
    :param phantom: Numpy 2d array
    :param perturbed_phantoms: Numpy 3d array
    :param utils_obj: Instance of Utils class
    :param setup_obj: Instance of Setup class
    :param sample_id: id for sample
    :param lock: Multiprocessing lock
    :return:
    """
    # path to weight file(s)
    weight_path = utils_obj.weight_path.format(setup_obj.K, setup_obj.P, setup_obj.R)

    # if setup_obj.IN_BATCHES:
    #     # weight filepath
    #     weight_file_template = weight_path + "_{}_{}.pkl"
    #     gen_sinogram = Sinogram(phantom=phantom, perturbed_phantoms=perturbed_phantoms, id_sample=sample_id,
    #                             nb_source_angles=setup_obj.K, batch_size=setup_obj.BATCH_SIZE, utils=utils_obj)
    #     # generate original and perturbed sinogram at the same time, this way weight batches have to only be loaded once
    #     gen_sinogram.generate_sinogram_batch(file_sinogram=setup_obj.SINO_NAME + str(sample_id),
    #                                          file_sinogram_perturbed=setup_obj.SINO_PERTURBED_NAME + str(sample_id),
    #                                          weight_file=weight_file_template, save_all=setup_obj.SAVE_ALL)
    # else:
    # load weight file
    weight_path = weight_path + ".npz"
    # load from npz file
    weights = Utils.load_npz_weights(weight_path, nb_angles=setup_obj.K, nb_detectors=2 * setup_obj.P + 1,
                                     fov_shape=(2 * setup_obj.R + 1, 2 * setup_obj.R + 1))
    gen_sinogram = Sinogram(phantom=phantom, perturbed_phantoms=perturbed_phantoms, id_sample=sample_id,
                            nb_source_angles=setup_obj.K, angle_ray_weights=weights, utils=utils_obj)
    # generate original and perturbed sinogram at the same time
    gen_sinogram.generate_sinograms(file_sinogram=setup_obj.SINO_NAME + str(sample_id),
                                    file_sinogram_perturbed=setup_obj.SINO_PERTURBED_NAME + str(sample_id),
                                    save_all=setup_obj.SAVE_ALL)

    # calculate error between perturbed sinogram and original (dependant variable)
    gen_sinogram.calculate_error(lock)
    return


def parallel(n_cores, function, jobs, setup_obj, path):
    """
    Execute function in parallel with n_cores many cores.

    :param n_cores: Number of cores to use
    :param function: Function to parallelise
    :param jobs: List of tuples with (id, path to data folder)
    :param path: Path to data folder
    :param setup_obj: Instance of Setup class
    :return:
    """
    lock_ph = multiprocessing.Lock()
    lock_si = multiprocessing.Lock()
    lock_eta = multiprocessing.Lock()
    _ = Parallel(n_jobs=n_cores, backend='threading')(
        delayed(function)(sample, path, setup_obj, lock_ph, lock_si, lock_eta) for sample in tqdm(jobs, desc='Generating data'))


def run_jobs(numb_cores, generate_samples, job_list, setup_obj, utils_obj, path):
    # parallelise the execution of all jobs in job_list
    parallel(numb_cores, generate_samples, job_list, setup_obj=setup_obj, path=path)


def create_jobs(n_samples, max_sample):
    """
    Create a list of length n_samples for parallelization, if some samples already exist, add on top of those
    :param n_samples: Number of samples to generate
    :param max_sample: Maximum id of samples that already exist
    :return:
    """
    list_samples = [s for s in range(n_samples + max_sample) if s not in range(max_sample)]
    return list_samples


def read_args():
    """
        Read execution parameters to generate -s many samples on -c many cores.

        Examples:
            * $ generateSamples.py --path C:Users\... --experiment_id 1 --cores 4 --samples 2000 --geometry parallel
    """
    parser = argparse.ArgumentParser(description='Process inputs for generating single phantom data.')
    parser.add_argument('--path', '-p', type=str, help='the path to the data directory.')
    parser.add_argument('--experiment_id', '-e', type=str, help='id of experiment to execute.')
    parser.add_argument('--cores', '-c', type=int, default=2, help='number of cores to use.')
    parser.add_argument('--samples', '-s', type=int, default=2, help='number of samples to generate.')
    parser.add_argument('--geometry', '-g', type=str, default='parallel', help='parallel or fan beam geometry')
    parser.add_argument('--save_all', default=False, action='store_true')
    # parser.add_argument('--no-save_all', dest='load_npz', action='store_false')

    arguments = parser.parse_args()
    return arguments


if __name__ == "__main__":
    """
    Generate Phantoms, Sinograms and pertubed Sinograms.
    -s number of samples, 
    -e experiment id, 
    --geometry parallel or fan,
    -p path to store data,
    -c number of cores to use
    """

    if len(sys.argv) < 9:
        print(
            'Missing arguments: generateSamples.py '
            '-c <number_cores> -s <number_samples> -p <data_path> -e <experiment_id> --geometry <parallel | fan>')
        print(f'Only received the following: {",".join(sys.argv)}')
        exit(1)
    args = read_args()
    print(f'I read the arguments: {args.cores} cores, '
          f'{args.samples} samples, '
          f'data path: {abspath(args.path)}, '
          f'experiment: {args.experiment_id} and '
          f'geometry: {args.geometry}.')

    if args.geometry == 'parallel':
        setup = SetupParallel(experiment_id=args.experiment_id, save_all=args.save_all)
    elif args.geometry == 'fan':
        setup = SetupFan(experiment_id=args.experiment_id, save_all=args.save_all)

    utils = Utils(args.path, setup.EXPERIMENT)
    # create all db files for phantoms, sinograms and etas
    utils.create_dbs(nb_detectors=2 * setup.P + 1, nb_angles=setup.K, fov_size=2 * setup.R + 1)

    logger, _ = utils.set_up_logging('debug', filename='gendata')
    logger_info, _ = utils.set_up_logging('info', filename='gendata')
    logger.debug('#' * 20)
    logger.debug(f'starting experiment {args.experiment_id} for {args.samples} samples on {args.cores} cores.')
    logger_info.info('#' * 20)
    logger_info.info(f'starting experiment {args.experiment_id} for {args.samples} samples on {args.cores} cores.')
    job_list = create_jobs(args.samples, max_sample=utils.get_max_sample_id())
    logger_info.info(f'Creating samples from: {min(job_list)} to {max(job_list)}.')

    start = datetime.now()
    # run jobs in parallel
    try:
        run_jobs(args.cores, generate_samples, job_list, setup_obj=setup, utils_obj=utils, path=args.path)
    except Exception as e:
        tables.file._open_files.close_all()
        raise e
    end = datetime.now()

    print('#' * 20)
    print(f'Parallel execution of {args.samples} on {args.cores} took: {(end-start).seconds} seconds.')
    logger_info.info(f'Parallel execution of {args.samples} on {args.cores} took: {(end-start).seconds} seconds.')
