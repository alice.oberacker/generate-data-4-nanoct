import os, sys
import numpy
import torch
import scipy.sparse

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from raytrafo.MatrixRayTrafo import MatrixRayTrafo


class SinogramSparse:

    def __init__(self, phantom=None, perturbed_phantoms=None, id_sample=None, weights_path=None, utils=None,
                 fov_shape=None, geometry_shape=None):
        self.phantom = phantom
        self.perturbed_phantom = perturbed_phantoms
        self.sinogram = None
        self.perturbed_sinogram = None
        self.weights = scipy.sparse.load_npz(weights_path + '.npz')
        self.trafo_obj = MatrixRayTrafo(self.weights, fov_shape, geometry_shape, order='C')
        self.geometry_shape = geometry_shape
        self.Utils = utils
        self.logger, self.lock = self.Utils.set_up_logging(level='debug', filename='gendata')
        self.sample_id = id_sample

    def generate_sinogram(self, phantom):
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'calculating sinogram for sample {self.sample_id}.')
        self.sinogram = self.trafo_obj(phantom).T
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'done calculating sinogram for sample {self.sample_id}.')
        return self.sinogram

    def generate_perturbed_sinogram(self, phantom_list):
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'calculating perturbed sinogram for sample {self.sample_id}.')
        perturbed_sino = numpy.zeros(self.geometry_shape)
        for i, p in enumerate(phantom_list):
            s_ = self.trafo_obj(phantom_list[i])
            # perturbed_sino[:, i] = s_[:, i]
            perturbed_sino[i, :] = s_[i, :]
        self.perturbed_sinogram = perturbed_sino.T
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'done calculating perturbed sinogram for sample {self.sample_id}.')
        return self.perturbed_sinogram

    def save_sinogram(self, sample_id, lock):
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'saving sinograms for sample {sample_id}.')
        self.Utils.save_sinogram_to_h5(sample_id, [self.sinogram, self.perturbed_sinogram], lock)
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'done saving sinograms for sample {sample_id}.')

    def calculate_error(self, lock):
        """
        Calculate the L2 norm (Frobenius norm) of the original minus the perturbed sinogram and store it to a file.
        :return: norm of sinogram difference
        """
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'calculating errors for sample {self.sample_id}.')
        dict = self.Utils.calc_errors(self.sinogram, self.perturbed_sinogram)
        self.logger.debug(f'done calculating errors for sample {self.sample_id}.')
        # only save the error if multiple samples are being created
        self.logger.debug(f'saving errors for sample {self.sample_id}.')
        if self.sample_id is not None:
            self.Utils.save_norm_to_file(self.sample_id, dict, lock, dim=self.sinogram.shape)
        self.Utils.logger_log(logger=self.logger, lock=self.lock,
                              text=f'done saving errors for sample {self.sample_id}.')
        return


class Sinogram:

    def __init__(self, phantom=None, perturbed_phantoms=None, id_sample=None, nb_source_angles=None, angle_index=None,
                 detector_index=None, angle_ray_weights=None, utils=None, verbose=False):
        """
        This class generates a sinogram given weights for each beta and each ray as well as a phantom.
        :param phantom: numpy array of phantom
        :param perturbed_phantoms: list of numpy arrays of perturbed phantoms
        :param id_sample: id to process in parallel mode
        :param nb_source_angles: number of scanner positions (beta or phi)
        :param angle_index: Specific angle, to calculate single angle-column of sinogram
        :param detector_index: Specific detector, to calculate single detector point of sinogram
        :param angle_ray_weights: list of weights for each angle and each ray
        ie 4 betas, 3 rays each:
        [
          [ [],[],[] ],
          [ [],[],[] ],
          [ [],[],[] ],
          [ [],[],[] ]
        ]
        """
        # only for non-batch processing
        self.angle_ray_weights = angle_ray_weights
        # array of phantom fov_size x fov_size
        self.phantom = phantom
        self.phantoms = perturbed_phantoms
        # shape of sinogram is defined by weights
        # case for batch processing, shape is calculated in runtime
        self.sinogram_shape = None
        if angle_ray_weights is not None:
            # non-batch processing, weights are passed in as param
            self.sinogram_shape = (len(self.angle_ray_weights[0]), len(self.angle_ray_weights))
        self.sinogram = None
        self.perturbed_sinogram = None

        # when sinogram only calculated for a single angle
        self.angle_index = angle_index
        # when sinogram only calculated for a single detector
        self.detector_index = detector_index
        self.nb_source_angles = nb_source_angles
        self.sample_id = id_sample
        self.Utils = utils
        self.verbose = verbose

    def generate_sinograms(self, file_sinogram='', file_sinogram_perturbed=''):
        """
        Calculate the sinogram. Each column in the matrix represents one beta and each row belongs to a ray.
        This function can generate a sinogram from a single image and from a sequence of images.

        :param file_sinogram:
        :param file_sinogram_perturbed:
        :param save_all: whether or not to save image
        :return: Sinogram (numpy array)
        """
        if self.phantom is not None:
            self.sinogram = self.create_sinogram_from_image()
        if self.phantoms is not None:
            self.perturbed_sinogram = self.create_sinogram_from_sequence()

        # save to pickle and img
        if (self.phantom is not None) and file_sinogram:
            self.Utils.save_to_pickle(self.sinogram, file_name=file_sinogram, obj_type='sinogram')
        if (self.phantoms is not None) and file_sinogram_perturbed:
            self.Utils.save_to_pickle(self.perturbed_sinogram, file_name=file_sinogram_perturbed, obj_type='sinogram')

    def create_sinogram_from_image(self):
        """
        Calculate the sinogram. Each column in the matrix represents one beta and each row belongs to a ray.
        This uses a single image of a phantom.
        :return: Sinogram (numpy array)
        """
        # create sinogram for all betas
        if self.angle_index is None:
            # for each beta one column, for each ray one entry in column
            sinogram = numpy.zeros(self.sinogram_shape)
            # iterate over betas
            for ii, rays_weights in enumerate(self.angle_ray_weights):
                # for each alpha (ray) get the weight matrix and calculate sum of (weight matrix * phantom)
                sinogram[:, ii] = self.calculate_sinogram(rays_weights, self.phantom)
        else:
            # calculate sinogram only for one beta
            # beta_index mod batch_size, ie, batch 8-15 with index 8 will be out of range, need index 0 instead (8mod8)
            weight = self.angle_ray_weights[self.angle_index]
            if self.detector_index is not None:
                weight = [weight[self.detector_index]]
            sinogram = self.calculate_sinogram(weight, self.phantom)
        return sinogram

    def create_sinogram_from_sequence(self, index_phantom=0):
        """
        Calculate the sinogram. Each column in the matrix represents one beta and each row belongs to a ray.
        This uses a sequence of perturbed phantoms, to simulate a motion of the object.
        :return: Sinogram (numpy array)
        """
        # for each beta one column, for each ray one entry in column
        sinogram = numpy.zeros(self.sinogram_shape)
        # iterate over betas
        for ii, rays_weights in enumerate(self.angle_ray_weights):
            # for each alpha get the weight matrix and calculate sum of (weight matrix * phantom)
            sinogram[:, ii] = self.calculate_sinogram(rays_weights, self.phantoms[ii+index_phantom])
        return sinogram

    @staticmethod
    def calculate_sinogram(ray_weight, phantom):
        """
        Calculate a single sinogram column (ie one beta).
        Multiply the weights of one ray with the phantom and sum across.
        Return inverted column to match parallel beam sinograms.
        :param ray_weight:
        :param phantom:
        :return:
        """
        if torch.is_tensor(phantom):
            sinogram_column = [torch.sum(wM.to_dense() * phantom) for wM in ray_weight]
        else:
            sinogram_column = [wM.multiply(phantom).sum() for wM in ray_weight]
        return sinogram_column  # [::-1]

    def calculate_error(self, lock):
        """
        Calculate the L2 norm (Frobenius norm) of the original minus the perturbed sinogram and store it to a file.
        :return: norm of sinogram difference
        """
        dict = self.Utils.calc_errors(self.sinogram, self.perturbed_sinogram)
        # only save the error if multiple samples are being created
        if self.sample_id is not None:
            self.Utils.save_norm_to_file(self.sample_id, dict, lock, dim=self.sinogram.shape)
        return
