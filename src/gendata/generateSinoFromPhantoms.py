import argparse
import datetime
import json
import multiprocessing
from joblib import Parallel, delayed
import os, sys
import numpy as np
import tables
from skimage import io, transform
from tqdm import tqdm
import platform

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from Setup_parallel import SetupParallel
from Setup_fanflat import SetupFanFlatASTRA as SetupFan
from UtilsData import Utils
from generateSamples import generate_fp


def load_phantoms_h5(path):
    """
    Load the phantom db from the h5 file.
    :param path: Absolute path to h5 file.
    :return: tables object
    """
    filter = tables.Filters(complib='zlib', complevel=5)
    f_phantoms = tables.open_file(path, mode='r', filters=filter)
    return f_phantoms


def yield_phantom(phantoms, ids):
    """
    This creates a generator that returns the correspondong phantom from the db object
    for each id in ids.
    :param phantoms: Tables object
    :param ids: List of ids to work through
    :return:
    """
    for i in ids:
        idx = np.where(i == phantoms.root.id.data[:])[0][0]
        yield phantoms.root.phantoms.original[idx]


def yield_perturbations(path, ids):
    """
    Load the affine transforms for each time step from the perturbation files.
    Each file has all time steps for one sample.
    :param path: path to file
    :param ids: sample id
    :return: list of perturbations for each id
    """
    for i in ids:
        path_ = os.path.join(path, f'phantom_perturbations_{i}.json')
        yield json.load(open(path_, 'rb'))


def convert_str_to_array(s):
    """
    Convert a JSON string to an array of floats.
    :param s: String
    :return: Array
    """
    return np.array(json.loads(s))


def yield_perturbed_phantoms(generator_phantoms, affine_transforms, num_angles=None, video=False):
    """
        Generator to apply transformations to phantoms.
        :param generator_phantoms: A generator that delivers unperturbed phantoms.
        :param affine_transforms: A generator that delivers affine transformations for each transformation.
        :return: original phantom and list of perturbed phantoms
        """
    for p in generator_phantoms:
        transforms = next(affine_transforms)
        # reduce the transformations down to the number of scanner positions in this geometry.
        if num_angles is not None:
            transforms = transforms[:num_angles]
        perturbed = []
        # for each transformation convert JSON string to array
        # and apply transform to phantom
        for tr in transforms:
            mat = convert_str_to_array(tr['affine_transform'])
            t = transform.AffineTransform(mat)
            new_p = transform.warp(p, inverse_map=t, cval=0)
            perturbed.append(new_p)
        # if video:
        #     create_video(perturbed, i)
        yield p, perturbed

#
# def create_video(images, id_):
#     """
#     Write an avi video from list of images
#     :param images: list of images (list of numpy arrays)
#     :param file_name: path to saved file's name
#     :return: None
#     """
#     path = os.path.join(f'./phantom_movement_{id_}' + ".avi")
#     # Get size of padded image
#     size = images[0].shape[:2]
#     # Frames per second
#     fps = 30
#
#     # Initialize writer
#     fourcc = VideoWriter_fourcc('M', 'J', 'P', 'G')
#     # out = VideoWriter(path, VideoWriter_fourcc(*'DIVX'), 30, size)
#     out = VideoWriter(path, fourcc, fps, size)
#     # Write video
#     for image in images:
#         # transform from float between 0 and 1 to int between 0 and 255
#         # frame = np_uint8(255 * image)
#         gray = normalize(image, None, 255, 0, norm_type=NORM_MINMAX, dtype=CV_8U)
#         gray_3c = merge([gray, gray, gray])
#         out.write(gray_3c)
#         # out.write(frame)
#     out.release()


def read_args():
    parser = argparse.ArgumentParser(description='Process inputs for generating single phantom data.')
    parser.add_argument('--path', '-p', type=str, help='the path to the data directory.')
    parser.add_argument('--experiment_id', '-e', type=str, help='id of experiment to execute.')
    parser.add_argument('--cores', '-c', type=int, default=2, help='number of cores to use.')
    parser.add_argument('--samples', '-s', type=int, default=None, help='number of samples to generate.')
    parser.add_argument('--geometry', '-g', type=str, default='parallel', help="'parallel' or 'fan' beam geometry.")

    arguments = parser.parse_args()
    return arguments


def copy_files(src=[], dst=[]):
    for s, d in zip(src, dst):
        if not os.path.exists(d) or (os.path.getsize(s) != os.path.getsize(d)):
            cmd = f'copy {s} {d}' if platform.system() == 'Windows' else f'cp {s} {d}'
            os.system(cmd)


if __name__ == "__main__":
    """
    Before executing this, you need to place the phantom_db.h5 file and the perturbation json files
    into the new experiment folder.
    """
    args = read_args()

    dst_path = os.path.join(args.path, f'experiment#{args.experiment_id}')
    if args.geometry == 'parallel':
        setup = SetupParallel(experiment_id=args.experiment_id)

    elif args.geometry == 'fan':
        setup = SetupFan(experiment_id=args.experiment_id)
    else:
        raise ValueError(f"'{args.geometry}' is not a valid geometry.")

    utils = Utils(args.path, setup.EXPERIMENT)

    try:
        # load phantom h5
        phantoms_h5 = load_phantoms_h5(os.path.join(dst_path, 'phantoms', 'phantom_db.h5'))
        # you could adjust ids_ in case you don't want to generate sinograms for all phantoms
        ids_ = phantoms_h5.root.id.data[:args.samples]
        # create phantom generator
        phantoms_generator = yield_phantom(phantoms_h5, ids_)
        # create perturbations generator
        affine_transform = yield_perturbations(os.path.join(dst_path, 'perturbations'), ids=ids_)
        # apply transforms to phantoms in generator, returns original and perturbed phantoms
        phantoms = yield_perturbed_phantoms(phantoms_generator, affine_transform, num_angles=setup.K)
    except OSError and FileNotFoundError:
        print('You need to first copy the phantom_db.h5 file and the perturbation files into the new project.')
        exit(1)

    # create all db files for phantoms, sinograms and etas
    utils.create_dbs(nb_detectors=2 * setup.P + 1, nb_angles=setup.K, fov_size=2 * setup.R + 1)
    # create locks for accessing sinograms and eta in parallel
    lock_si = multiprocessing.Lock()
    lock_eta = multiprocessing.Lock()

    # generate new sinograms and save to db
    start = datetime.datetime.now()
    _ = Parallel(n_jobs=args.cores, backend='threading')(
        delayed(generate_fp)(*next(phantoms), utils, setup, sample, lock_si,
                             lock_eta) for sample in tqdm(ids_, desc='Generating Sinograms'))
    end = datetime.datetime.now()

    # close phantom file
    phantoms_h5.close()
    print(f'create sinograms in {(end - start).total_seconds()} secs')



