import numpy
from numba import jit


@jit(nopython=True)
def rotate_vector(vector, alpha):
    """
    Rotate a vector around angle alpha
    :param vector: 2d vector of floats
    :param alpha: angle (float)
    :return: rotated vector
    """
    cosd = numpy.cos(numpy.deg2rad(alpha))
    sind = numpy.sin(numpy.deg2rad(alpha))
    rotation_matrix = numpy.array([[cosd, -sind],
                                   [sind,  cosd]])
    vector_rot = rotation_matrix @ vector
    return vector_rot


@jit(nopython=True)
def calc_pixel_center_rotation(pixel_size, x_center, y_center, rotation_angle, ii, jj):
    """
    :param pixel_size: The size of a pixel (float)
    :param x_center:  Center of pixel x direction
    :param y_center:  Center of pixel y direction
    :param rotation_angle: angle to rotate the feature (float)
    :param ii: step (int)
    :param jj: step (int)
    :return: vector of pixels centers and rotated vector
    """
    # Calculate pixel center
    x = -1 + pixel_size * jj + pixel_size / 2
    y = -1 + pixel_size * ii + pixel_size / 2
    vector = numpy.array([x, y])

    # Calculate rotated vector
    v_rot = rotate_vector(vector - numpy.array([x_center, y_center]), -rotation_angle)\
        + numpy.array([x_center, y_center])
    return v_rot


@jit(nopython=True)
def get_rectangle_feature(image_size, pixel_size, x_center, y_center, x_size, y_size, rotation_angle, graylevel):
    """
    Generate a rectangle phantom feature
    :param image_size: Size of the image (int)
    :param pixel_size: Size of 1 pixel (float)
    :param x_center: Center of feature (x direction) (float)
    :param y_center: Center of feature (y direction) (float)
    :param x_size: Size of feature (x direction) (float)
    :param y_size: Size of feature (y direction) (float)
    :param rotation_angle: Rotation of the feature (float)
    :param graylevel: Graylevel of the feature (float between 0 and 1)
    :return: Rectangle phantom feature (np.array)
    """
    # Calculate half the size as parameter to avoid repeated calculation
    x_size_by_2 = x_size / 2
    y_size_by_2 = y_size / 2

    # Initialize empty phantom feature
    phantom_feature = numpy.zeros((image_size, image_size))

    # iterate over rows and columns
    for ii in range(image_size):
        for jj in range(image_size):
            # get rotation vector
            v_rot = calc_pixel_center_rotation(pixel_size, x_center, y_center, rotation_angle, ii, jj)

            # rectangle condition
            if (x_center - x_size_by_2 <= v_rot[0]) \
                    and (v_rot[0] <= x_center + x_size_by_2) \
                    and (y_center - y_size_by_2 <= v_rot[1]) \
                    and (v_rot[1] <= y_center + y_size_by_2):
                # if in rectangle set graylevel
                phantom_feature[ii, jj] = graylevel

    return phantom_feature


@jit(nopython=True)
def get_ellipse_feature(image_size, pixel_size, x_center, y_center, x_size, y_size, rotation_angle, graylevel):
    """
    Generate an ellipse phantom feature
    :param image_size: Size of the image (int)
    :param pixel_size: Size of 1 pixel (float)
    :param x_center: Center of feature (x direction) (float)
    :param y_center: Center of feature (y direction) (float)
    :param x_size: Size of feature (x direction) (float)
    :param y_size: Size of feature (y direction) (float)
    :param rotation_angle: Rotation of the feature (float)
    :param graylevel: Graylevel of the feature (float between 0 and 1)
    :return: Ellipse phantom feature (np.array)
    """
    # Calculate half the size as parameter to avoid repeated calculation
    x_size_by_2 = x_size / 2
    y_size_by_2 = y_size / 2

    x_size_by_2_squared = x_size_by_2 ** 2
    y_size_by_2_squared = y_size_by_2 ** 2

    # Initialize empty phantom feature
    phantom_feature = numpy.zeros((image_size, image_size))

    # iterate over rows and columns
    for ii in range(image_size):
        for jj in range(image_size):
            # get rotation vector
            v_rot = calc_pixel_center_rotation(pixel_size, x_center, y_center, rotation_angle, ii, jj)

            # rectangle condition
            if (v_rot[0] - x_center) ** 2 / x_size_by_2_squared + (v_rot[1] - y_center) ** 2 / y_size_by_2_squared <= 1:
                # if in rectangle set graylevel
                phantom_feature[ii, jj] = graylevel

    return phantom_feature


@jit(nopython=True)
def get_phantom_feature(image_size, pixel_size, x_center, y_center, x_size, y_size, rotation_angle, shape, graylevel):
    """
    Generate a phantom feature.
    :param image_size: Size of the image (int)
    :param pixel_size: Size of 1 pixel (float)
    :param x_center: Center of feature (x direction) (float)
    :param y_center: Center of feature (y direction) (float)
    :param x_size: Size of feature (x direction) (float)
    :param y_size: Size of feature (y direction) (float)
    :param rotation_angle: Rotation of the feature (float)
    :param shape: Kind of shape to be returned (string)
    :param graylevel: Graylevel of the feature (float between 0 and 1)
    :return: Phantom feature (as in 'shape')
    """
    # Call different functions according to
    if shape == "rectangle":
        phantom_feature = get_rectangle_feature(image_size, pixel_size, x_center, y_center, x_size, y_size,
                                                rotation_angle, graylevel)

    elif shape == "ellipse":
        phantom_feature = get_ellipse_feature(image_size, pixel_size, x_center, y_center, x_size, y_size,
                                              rotation_angle, graylevel)
    # if shape is not implemented, return empty phantom_feature
    else:
        phantom_feature = numpy.zeros((image_size, image_size))

    return phantom_feature
