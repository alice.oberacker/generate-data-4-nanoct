import os, sys
from numpy import identity as np_identity
import numpy
import logging

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from Transformations import Transformation
from Movement import Movement


class PerturbedPhantom:

    def __init__(self, phantom, resolution=200, movement={}):
        """
        Generate a series of perturbed phantoms from an original phantom.
        :param phantom: skimage image object (numpy array)
        :param resolution:
        :param number_transforms:
        :param shear: strength of shear
        :param rotation: strength of rotation
        :param translation_x: strength of translation x direction
        :param translation_y: strength of y direction
        :param with_sinus:
        :param rel_max_shift:
        """
        self.phantom = phantom
        self.resolution = resolution
        self.fov_size = 2 * resolution + 1
        self.movement_types = movement.keys()

        self.number_transforms = movement['number_transforms']  # len(betas)-1, because the original phantom is on position 0
        self.movement = Movement(movement)


    @staticmethod
    def start_logger_if_necessary():
        logger = logging.getLogger("mylogger")
        if len(logger.handlers) == 0:
            logger.setLevel(logging.INFO)
            sh = logging.StreamHandler()
            sh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
            fh = logging.FileHandler('out.log', mode='w')
            fh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
            logger.addHandler(sh)
            logger.addHandler(fh)
        return logger

    def create_sequence(self, normalize=True, norm=1, return_params=True):
        """
        Create a sequence of images based on the original phantom and the given translation parameters.
        :param normalize: Bool. If true, normalize the matrix in each step
        :param norm: norm for normalization of matrix (float)
        :param return_params: If true, return the transform parameters as a list of arrays of shape (3,3)
        :return: list of transformed images (starting with the original image), if return_params list of parameters
        """
        image_shape = self.phantom.shape[:2]
        # Call function to generate random transformations
        self.movement.generate_movement()
        # array with #beta many phantoms
        images = numpy.zeros((self.number_transforms+1, *image_shape))
        # list of transform parameters for each image
        params = [None] * (self.number_transforms+1)
        # start perturbed phantoms with original phantom
        images[0] = self.phantom
        params[0] = np_identity(3)
        # Create 3x3 identity matrix
        matrix = np_identity(3)

        # Generate the transformed images
        for ii in range(self.number_transforms):
            # first apply rotation
            image_tmp, param_rot = self.apply_rotation_or_translation(ii, matrix, image=self.phantom,
                                                                      movement='rotation',
                                                                      normalize=normalize, norm=norm)
            # then apply translation, so that phantom is rotated around center
            images[ii + 1], params_trl = self.apply_rotation_or_translation(ii, matrix, image=image_tmp,
                                                                            movement='translation',
                                                                            normalize=normalize, norm=norm)
            # combine rotation matrix with translation matrix
            params[ii + 1] = param_rot @ params_trl

            # if you want to accumulate the translations (not returning back to (0,0) ) then use param_rot @ param_trl
            # instead of the identity matrix
            # matrix = param_rot @ params_trl
        return (images, params) if return_params else images

    def apply_rotation_or_translation(self, ii, matrix, image, movement, normalize=True, norm=1):
        # Create Transformation object
        if movement == 'rotation':
            transformer = Transformation(self.resolution,
                                         rotation=self.movement.random_rotations[ii],
                                         shear=0,
                                         translations=0
                                         )
        elif movement == 'translation':
            transformer = Transformation(self.resolution,
                                         rotation=0,
                                         shear=self.movement.random_shears[ii],
                                         translations=self.movement.random_transls[ii]
                                         )

        # new transformation matrix
        affine_transform_next, matrix = transformer.generate_transformation_matrix(matrix, normalize=normalize,
                                                                                  norm=norm)
        # Apply transform to original image and append to list of images
        image = transformer.apply_transform(image, affine_transform_next)
        # param = affine_transform_next.params.copy()
        return image, matrix

