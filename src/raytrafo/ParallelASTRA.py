from functools import partial
import numpy as np
import scipy.sparse
import astra
from tqdm import tqdm

class AstraLinkedFP:
    def __init__(self, proj_geom, vol_geom, vol_array, sinogram_array):
        if proj_geom['type'] in ['parallel', 'parallel_vec']:
            PROJ_NAME = 'cuda'
            ALG_NAME = 'FP_CUDA'
            astra_data_module = astra.data2d
        elif proj_geom['type'] in ['parallel3d', 'parallel3d_vec']:
            PROJ_NAME = 'cuda3d'
            ALG_NAME = 'FP3D_CUDA'
            astra_data_module = astra.data3d
        else:
            raise NotImplementedError

        self.proj_id = astra.create_projector(PROJ_NAME, proj_geom, vol_geom)

        vol_id = astra_data_module.link('-vol', vol_geom, vol_array)
        sino_id = astra_data_module.link('-sino', proj_geom, sinogram_array)
        cfg_fp = astra.astra_dict(ALG_NAME)
        cfg_fp['VolumeDataId'] = vol_id
        cfg_fp['ProjectionDataId'] = sino_id
        self.alg_id = astra.algorithm.create(cfg_fp)

    def __enter__(self):
        return self.alg_id

    def __exit__(self, type, value, traceback):
        astra.projector.delete(self.proj_id)
        astra.algorithm.delete(self.alg_id)

# definition of equidistant angles
# If use_angles_like_odl, angles are shifted by half a step, and -pi/2 is
# subtracted, which is suitable for implicit xy-transposition (passing
# xy volume data despite astra expecting yx volume data; cf.
# get_ray_trafo_matrix_parallel_2d_astra)
def get_angles_parallel(num_angles, use_angles_like_odl=False):
    angles = np.linspace(0, np.pi, num_angles, False)
    if use_angles_like_odl:
        angles = (angles + 0.5 * np.pi/num_angles) - 0.5*np.pi
    return angles


# dynamic default parameters

def _get_default_im_shape_and_px_size(ndim, im_shape, im_px_size=None):
    if np.isscalar(im_shape):
        im_shape = (im_shape,) * ndim
    if im_px_size is None:
        im_px_size = 2. / max(im_shape)
    if np.isscalar(im_px_size):
        im_px_size = (im_px_size,) * ndim
    return im_shape, im_px_size

_get_default_im_shape_and_px_size_2d = partial(_get_default_im_shape_and_px_size, 2)
_get_default_im_shape_and_px_size_3d = partial(_get_default_im_shape_and_px_size, 3)

def _get_default_det_px_horz_size(det_col_count, det_px_horz_size=None):
    if det_px_horz_size is None:
        det_px_horz_size = 2. * np.sqrt(2.) / det_col_count
    return det_px_horz_size


# volume and projection geometries (ASTRA dicts)

def get_parallel_2d_vol_geom(im_shape, im_px_size=None):
    im_shape, im_px_size = _get_default_im_shape_and_px_size_2d(im_shape, im_px_size=im_px_size)

    vol_geom = astra.create_vol_geom(*im_shape)  # (y, x); (rows, columns)
    vol_geom['option']['WindowMinX'] = vol_geom['option']['WindowMinX'] * im_px_size[1]
    vol_geom['option']['WindowMaxX'] = vol_geom['option']['WindowMaxX'] * im_px_size[1]
    vol_geom['option']['WindowMinY'] = vol_geom['option']['WindowMinY'] * im_px_size[0]
    vol_geom['option']['WindowMaxY'] = vol_geom['option']['WindowMaxY'] * im_px_size[0]
    return vol_geom

def get_parallel_3d_vol_geom(im_shape, im_px_size=None):
    im_shape, im_px_size = _get_default_im_shape_and_px_size_3d(im_shape, im_px_size=im_px_size)
    # image coordinates: (z, y, x)

    vol_geom = astra.create_vol_geom(im_shape[1], im_shape[2], im_shape[0])  # (y, x, z); (rows, columns, slices)
    vol_geom['option']['WindowMinX'] = vol_geom['option']['WindowMinX'] * im_px_size[2]
    vol_geom['option']['WindowMaxX'] = vol_geom['option']['WindowMaxX'] * im_px_size[2]
    vol_geom['option']['WindowMinY'] = vol_geom['option']['WindowMinY'] * im_px_size[1]
    vol_geom['option']['WindowMaxY'] = vol_geom['option']['WindowMaxY'] * im_px_size[1]
    vol_geom['option']['WindowMinZ'] = vol_geom['option']['WindowMinZ'] * im_px_size[0]
    vol_geom['option']['WindowMaxZ'] = vol_geom['option']['WindowMaxZ'] * im_px_size[0]
    return vol_geom

def get_parallel_2d_proj_geom(angles, det_shape, det_px_size=None, angle_offset=0., reverse_angles=False):
    det_px_size = _get_default_det_px_horz_size(det_shape)

    if angle_offset:
        angles = angles + angle_offset  # useful to undo the "- 0.5*np.pi" included in get_angles_parallel(..., use_angles_like_odl=True)
    if reverse_angles:
        angles = -angles

    proj_geom = astra.create_proj_geom('parallel', det_px_size, det_shape, angles)
    return proj_geom

def get_parallel_3d_proj_geom(angles, det_row_count, det_col_count, det_px_vert_size, det_px_horz_size=None, angle_offset=0., reverse_angles=True):
    det_px_horz_size = _get_default_det_px_horz_size(det_col_count)

    angles = np.asarray(angles)
    num_angles = len(angles)

    if angle_offset:
        angles = angles + angle_offset  # useful to undo the "- 0.5*np.pi" included in get_angles_parallel(..., use_angles_like_odl=True)
    if reverse_angles:
        angles = -angles

    proj_geom = astra.create_proj_geom('parallel3d', det_px_horz_size, det_px_vert_size, det_row_count, det_col_count, angles)
    return proj_geom

def get_parallel_3d_vec_proj_geom(angles, det_row_count, det_col_count, det_px_vert_size, det_px_horz_size=None, swap_uv=False, angle_offset=0., reverse_angles=True):
    det_px_horz_size = _get_default_det_px_horz_size(det_col_count)

    row_inds, col_inds = [9, 10, 11], [6, 7, 8]

    if swap_uv:
        det_row_count, det_col_count = det_col_count, det_row_count
        row_inds, col_inds = col_inds, row_inds

    angles = np.asarray(angles)
    num_angles = len(angles)

    if angle_offset:
        angles = angles + angle_offset  # useful to undo the "- 0.5*np.pi" included in get_angles_parallel(..., use_angles_like_odl=True)
    if reverse_angles:
        angles = -angles

    sin_angles = np.sin(angles)
    cos_angles = np.cos(angles)

    vectors = np.zeros((num_angles, 12))
    # rayX, rayY, rayZ
    vectors[:, 0] = sin_angles
    vectors[:, 1] = -cos_angles
    # vectors[:, 2] = 0.
    # dX, dY, dZ
    vectors[:, 3] = sin_angles
    vectors[:, 4] = -cos_angles
    # vectors[:, 5] = 0.
    # rows step (uX, uY, uZ if not swap_uv else vX, vY, vZ)
    # vectors[:, row_inds[0]] = 0.
    # vectors[:, row_inds[1]] = 0.
    vectors[:, row_inds[2]] = det_px_vert_size
    # column step (vX, vY, vZ if not swap_uv else uX, uY, uZ)
    vectors[:, col_inds[0]] = det_px_horz_size * cos_angles
    vectors[:, col_inds[1]] = det_px_horz_size * sin_angles
    # vectors[:, col_inds[2]] = 0.

    proj_geom = astra.create_proj_geom('parallel3d_vec', det_row_count, det_col_count, vectors)
    return proj_geom


# forward ray transform functions

def apply_ray_trafo_parallel_2d_astra(x, angles, det_shape, im_px_size=None, det_px_size=None, vec=False, vol_indexing='yx', proj_indexing='au'):
    det_shape = np.array(det_shape).item()
    assert vol_indexing in ['xy', 'yx']

    x = np.ascontiguousarray(x, dtype=np.float32)

    num_angles = len(angles)

    proj_shape = (num_angles, det_shape)

    # use angles to adapt to different image coordinates (avoid to transpose the array)
    if vol_indexing == 'xy':
        angle_offset = 0.
        reverse_angles = False  # the angle axis for parallel 2d appears to be inverted compared to 3d, due to the implicit xy-transposition it needs to be inverted again, so in total no inversion is needed
    else:  # 'yx':
        angle_offset = 0.5 * np.pi  # undo the "-0.5*np.pi" from get_angles_parallel(..., use_angles_like_odl=True), because data is already in native yx order
        reverse_angles = True  # the angle axis for parallel 2d appears to be inverted compared to 3d

    im_shape = x.shape
    vol_geom = get_parallel_2d_vol_geom(im_shape, im_px_size=im_px_size)
    proj_geom = get_parallel_2d_proj_geom(angles, det_shape, det_px_size=det_px_size, angle_offset=angle_offset, reverse_angles=reverse_angles)
    if vec:
        proj_geom = astra.functions.geom_2vec(proj_geom)

    projs = np.zeros(proj_shape, dtype=np.float32)

    with AstraLinkedFP(proj_geom, vol_geom, x, projs) as alg_id:
        astra.algorithm.run(alg_id)

    proj_transpose_axes = ['au'.index(d) for d in proj_indexing]  # determine tranposition to obtain requested coordinates from au coordinates produced by ASTRA

    projs = np.transpose(projs, axes=proj_transpose_axes)

    return projs

def apply_ray_trafo_parallel_3d_astra(x, angles, det_row_count, det_col_count, im_px_size=None, det_px_size=(None, None), vec=True, vol_indexing='zyx', proj_indexing='auv'):
    # move z to dim 0
    input_z_dim = vol_indexing.index('z')
    x = np.ascontiguousarray(np.moveaxis(x, input_z_dim, 0), dtype=np.float32)
    vol_indexing = 'z' + vol_indexing[:input_z_dim] + vol_indexing[input_z_dim+1:]
    assert vol_indexing in ['zxy', 'zyx']

    # vol_transpose_axes = [vol_indexing.index(d) for d in 'zyx']  # determine tranposition to obtain zyx coordinates for ASTRA

    # x = np.ascontiguousarray(np.transpose(x, axes=vol_transpose_axes), dtype=np.float32)

    num_angles = len(angles)

    swap_uv = vec  # for better memory layout (see comment in odl.astra_setup.astra_parallel_3d_geom_to_vec); only supported with parallel3d_vec

    proj_shape = (det_col_count, num_angles, det_row_count) if swap_uv else (det_row_count, num_angles, det_col_count)

    # use angles to adapt to different image coordinates (avoid to transpose the array)
    if vol_indexing[1:] == 'xy':
        angle_offset = 0.
        reverse_angles = True  # the angle axis needs to be inverted due to the implicit xy-transposition
    else:  # 'yx':
        angle_offset = 0.5 * np.pi  # undo the "-0.5*np.pi" from get_angles_parallel(..., use_angles_like_odl=True), because data is already in native yx order
        reverse_angles = False

    im_shape = x.shape
    _, im_px_size = _get_default_im_shape_and_px_size_3d(im_shape, im_px_size=im_px_size)
    vol_geom = get_parallel_3d_vol_geom(im_shape, im_px_size=im_px_size)

    det_px_vert_size, det_px_horz_size = det_px_size
    if det_px_vert_size is None:
        det_px_vert_size = im_px_size[0]  # set detector row height to height (z-step) of voxel

    if vec:
        proj_geom = get_parallel_3d_vec_proj_geom(angles, det_row_count, det_col_count, det_px_vert_size=det_px_vert_size, det_px_horz_size=det_px_horz_size, swap_uv=swap_uv, angle_offset=angle_offset, reverse_angles=reverse_angles)
    else:
        proj_geom = get_parallel_3d_proj_geom(angles, det_row_count, det_col_count, det_px_vert_size=det_px_vert_size, det_px_horz_size=det_px_horz_size, angle_offset=angle_offset, reverse_angles=reverse_angles)

    projs = np.zeros(proj_shape, dtype=np.float32)

    with AstraLinkedFP(proj_geom, vol_geom, x, projs) as alg_id:
        astra.algorithm.run(alg_id)

    proj_transpose_axes = [('uav' if swap_uv else 'vau').index(d) for d in proj_indexing]  # determine tranposition to obtain requested coordinates from coordinates produced by ASTRA

    projs = np.transpose(projs, axes=proj_transpose_axes)

    return projs


# matrix representation for 2d
def get_ray_trafo_matrix_parallel_2d_astra(im_shape, angles, det_shape, im_px_size=None, det_px_size=None, flatten=True, order='C', sparse=True, show_pbar=False):
    if np.isscalar(im_shape):
        im_shape = (im_shape, im_shape)
    det_shape = np.array(det_shape).item()

    num_angles = len(angles)

    proj_shape = (num_angles, det_shape)

    vol_geom = get_parallel_2d_vol_geom(im_shape, im_px_size=im_px_size)
    proj_geom = get_parallel_2d_proj_geom(angles, det_shape, det_px_size=det_px_size)

    x = np.zeros(im_shape, dtype=np.float32)
    projs = np.zeros(proj_shape, dtype=np.float32)

    CHUNK_MEMSIZE = 10000000  # 100MB

    chunk_size = int(CHUNK_MEMSIZE / 4 / np.prod(proj_shape))

    matrix_chunk_list = []

    matrix_chunk = np.zeros((np.prod(proj_shape), chunk_size), dtype='float32')
    cnt = 0

    with AstraLinkedFP(proj_geom, vol_geom, x, projs) as alg_id:
        for i in tqdm(range(np.prod(im_shape)), desc='assembling ray trafo via astra', disable=not show_pbar):
            idx = np.unravel_index(i, im_shape, order=order)
            x[idx] = 1.
            astra.algorithm.run(alg_id)
            x[idx] = 0.
            matrix_chunk[:, i-cnt] = projs.flat
            if (i+1) % chunk_size == 0 or i+1 == np.prod(im_shape):
                num_cols = i+1-cnt
                matrix_chunk_list.append(scipy.sparse.csr_matrix(matrix_chunk[:, :num_cols], copy=True) if sparse else matrix_chunk[:, :num_cols].copy())
                cnt += num_cols
                matrix_chunk[:] = 0.  # re-initialize

    if sparse:
        matrix = scipy.sparse.hstack(matrix_chunk_list).tocsr()
    else:
        matrix = np.hstack(matrix_chunk_list)

    assert matrix.shape[1] == np.prod(im_shape)

    if not flatten:
        matrix = matrix.reshape(proj_shape + im_shape, order=order)

    return matrix
