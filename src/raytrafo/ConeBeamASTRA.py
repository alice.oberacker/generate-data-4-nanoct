import os
import sys
from functools import partial
import numpy as np
import scipy.sparse
import astra
from tqdm import tqdm
try:
    import torch
    TORCH_AVAILABLE = True
except ImportError:
    TORCH_AVAILABLE = False

from .UtilsASTRA import AstraLinkedFP, AstraLinkedBP, get_2d_vol_geom, get_3d_vol_geom, get_default_im_shape_and_px_size_2d, get_default_im_shape_and_px_size_3d, get_xy_diagonal
from .RayTrafo import RayTrafo
if TORCH_AVAILABLE:
    from .UtilsTorchASTRA import TorchDirectASTRARayTrafoModule


def get_angles_cone_beam(num_angles, use_angles_like_odl=False):
    """
    Return equidistant angles.

    Parameters
    ----------
    num_angles : int
        Number of angles
    use_angles_like_odl : bool
        If `True`, angles are shifted by half a step; the returned angle
        values then equal ``odl_geometry.angles`` for an equivalent ODL geometry
        obtained with :func:`odl.tomo.cone_beam_geometry`.
    """
    angles = np.linspace(0, 2. * np.pi, num_angles, False)
    if use_angles_like_odl:
        angles = angles + np.pi/num_angles
    return angles


# dynamic default parameters

def get_default_det_col_count_cone_beam(src_radius, det_radius, im_px_size_yx, xy_diagonal=2.*np.sqrt(2.)):
    """
    Return default number of detector columns like
    :func:`odl.tomo.cone_beam_geometry`, fulfilling the Nyquist criterion if
    equally spaced on a detector tightly spanning the maximum extent
    :param:`xy_diagonal` of the volume in the xy-plane, measured at the origin
    parallel to the detector plane.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    im_px_size_yx : 2- or 3-tuple of float
        Image voxel size in (z)yx order.
        If a 3-tuple is specified, the value `im_px_size_yx[0]` will be ignored.
    xy_diagonal : float, optional
        Maximum extent of the volume in the xy-plane, measured at the origin
        parallel to the detector plane. The default is ``2. * sqrt(2.)``.
    """
    # adapted from odl.tomo.cone_beam_geometry()
    if len(im_px_size_yx) == 3:
        im_px_size_yx = im_px_size_yx[1:]
    w = xy_diagonal * (src_radius + det_radius) / src_radius  # detector width
    omega = np.pi / min(im_px_size_yx[0], im_px_size_yx[1])
    r = src_radius + det_radius
    rb = np.sqrt(r**2 + (0.5 * w)**2)
    det_col_count = 2 * int(np.ceil(w * omega * r / (2 * np.pi * rb))) + 1
    return det_col_count

def get_default_det_px_horz_size_cone_beam(src_radius, det_radius, det_col_count, xy_diagonal=2.*np.sqrt(2.)):
    """
    Return default detector column spacing spanning the maximum extent
    :param:`xy_diagonal` of the volume in the xy-plane, measured at the origin
    parallel to the detector plane.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    det_col_count : int
        Number of detector columns to distribute.
    xy_diagonal : float, optional
        Maximum extent of the volume in the xy-plane, measured at the origin
        parallel to the detector plane. The default is ``2. * sqrt(2.)``.
    """
    # adapted from odl.tomo.cone_beam_geometry()
    det_px_horz_size = (xy_diagonal * (src_radius + det_radius) / src_radius) / det_col_count
    return det_px_horz_size

def get_default_det_px_vert_size_cone_beam(src_radius, det_radius, im_px_size_z, xy_diagonal=2.*np.sqrt(2.)):
    """
    Return default detector row spacing similarly to the default of
    :func:`odl.tomo.cone_beam_geometry` (quote from their source: "vertical
    spacing from the reco space, corrected for magnification at the 'back' of
    the object, i.e., where it is minimal"); the spacing however is different if
    a non-default number of detector rows is specified, in which case
    :func:`odl.tomo.cone_beam_geometry` adapts the spacing to cover the full
    volume extent in z dimension.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    im_px_size_z : float
        Image voxel size in z dimension.
    xy_diagonal : float, optional
        Maximum extent of the volume in the xy-plane, measured at the origin
        parallel to the detector plane. The default is ``2. * sqrt(2.)``.
    """
    # adapted from odl.tomo.cone_beam_geometry()
    rho = 0.5 * xy_diagonal  # corner-to-center distance in xy plane
    # set detector row height to height (z-step) of voxel times minimum magnification (cf. odl.tomo.cone_beam_geometry)
    det_px_vert_size = (src_radius + det_radius) / (src_radius + rho) * im_px_size_z
    return det_px_vert_size

def get_default_det_row_count_cone_beam(src_radius, det_radius, im_shape, im_px_size, det_px_vert_size=None):
    """
    Return default number of detector rows spanning the full volume extent in z
    dimension.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    im_shape : 3-tuple of int
        Shape of the image (in pixels): `(n_z, n_y, n_x)`.
    im_px_size : 3-tuple of float
        Image voxel size in zyx order.
    det_px_vert_size : float, optional
        Vertical detector pixel size (row spacing).
        If `None`, it is determined by
        :func:`get_default_det_px_vert_size_cone_beam` with `xy_diagonal`
        being calculated from :param:`im_shape` and :param:`im_px_size`.
    """
    # adapted from odl.tomo.cone_beam_geometry()
    xy_diagonal = get_xy_diagonal(im_shape, im_px_size)
    if det_px_vert_size is None:
        det_px_vert_size = get_default_det_px_vert_size_cone_beam(src_radius, det_radius, im_px_size[0], xy_diagonal=xy_diagonal)
    rho = 0.5 * xy_diagonal  # corner-to-center distance in xy plane
    dist = src_radius - rho
    half_cone_angle = np.arctan(0.5 * im_shape[0] * im_px_size[0] / dist)
    h = 2 * np.sin(half_cone_angle) * (src_radius + det_radius)
    det_row_count = int(np.ceil(h / det_px_vert_size))
    return det_row_count

# projection geometries (ASTRA dicts)

def get_cone_beam_proj_geom(src_radius, det_radius, angles, det_row_count, det_col_count, det_px_vert_size, det_px_horz_size=None, angle_offset=0., reverse_angles=False):
    """
    Create a cone-beam projection geometry of ASTRA type `'cone'`
    (with the rotation axis being orthogonal to the xy plane and in the center
    of the volume).

    Regarding the different angle conventions in ODL and ASTRA, consider the
    following two use cases:

        A)  `'zxy'` volume data should be projected without explicit
            transposition to ASTRA's native `'zyx'` order (intentionally using
            ASTRA's `'cone'` geometry with swapped-order volume data).
            To obtain projections matching those from an ODL geometry with
            the same angle values ``angles == odl_geometry.angles``, specify
            ``angle_offset=-1.5*np.pi, reverse_angles=True`` and flip the
            obtained projections along the horizontal detector axis; note that
            the :class:`odl.tomo.RayTransform` is called on `'xyz'` volume data.
        B)  `'zyx'` volume data should be projected, simply following all
            conventions of the ASTRA `'cone'` geometry.

    In both cases, to use angles like :func:`odl.tomo.cone_beam_geometry`, pass
    ``get_angles_cone_beam(..., use_angles_like_odl=True)`` as the
    :param:`angles` argument of this function.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    angles : array-like
        Rotation angles.
        Angles follow the convention of ASTRA's `'cone'` geometry.
    det_row_count : int
        Number of detector pixel rows.
    det_col_count : int
        Number of detector pixel columns.
    det_px_vert_size : float
        Vertical detector pixel size (row spacing).
    det_px_horz_size : float, optional
        Horizontal detector pixel size (column spacing).
        If `None`, it is determined by
        :func:`get_default_det_px_horz_size_cone_beam` assuming the default
        ``xy_diagonal=2.*np.sqrt(2.)``.
    angle_offset : float, optional
        Constant value to add to every rotation angle. The default is `0.`.
    reverse_angles : bool, optional
        If `True`, multiply each angle with `-1.`. The default is `False`.
    """
    if det_px_horz_size is None:
        det_px_horz_size = get_default_det_px_horz_size_cone_beam(src_radius, det_radius, det_col_count)

    angles = np.asarray(angles)
    num_angles = len(angles)

    if angle_offset:
        angles = angles + angle_offset
    if reverse_angles:
        angles = -angles

    proj_geom = astra.create_proj_geom('cone', det_px_horz_size, det_px_vert_size, det_row_count, det_col_count, angles, src_radius, det_radius)
    return proj_geom

def get_cone_beam_vectors(src_radius, det_radius, angles, det_px_vert_size, det_px_horz_size, swap_uv=False, angle_offset=0., reverse_angles=False):
    """
    Return vectors for an ASTRA `'cone_vec'` geometry (with the rotation axis
    being orthogonal to the xy plane and in the center of the volume).

    See the documentation of :func:`get_cone_beam_proj_geom` for the
    parameters not documented below.

    Parameters
    ----------
    det_px_horz_size : float
        Horizontal detector pixel size (column spacing).
        This parameter is required (unlike for :func:`get_cone_beam_proj_geom`).
    swap_uv : bool, optional
        If `True`, swap detector rows and columns, resulting in projection data
        with dimension order `'uav'`.
        The default is `False`, resulting in dimension order `'vau'`.
    """
    row_inds, col_inds = [9, 10, 11], [6, 7, 8]
    if swap_uv:
        row_inds, col_inds = col_inds, row_inds

    angles = np.asarray(angles)
    num_angles = len(angles)

    if angle_offset:
        angles = angles + angle_offset
    if reverse_angles:
        angles = -angles

    sin_angles = np.sin(angles)
    cos_angles = np.cos(angles)

    vectors = np.zeros((num_angles, 12))
    # rayX, rayY, rayZ
    vectors[:, 0] = src_radius * sin_angles
    vectors[:, 1] = -src_radius * cos_angles
    # vectors[:, 2] = 0.
    # dX, dY, dZ
    vectors[:, 3] = -det_radius * sin_angles
    vectors[:, 4] = det_radius * cos_angles
    # vectors[:, 5] = 0.
    # rows step (uX, uY, uZ if not swap_uv else vX, vY, vZ)
    # vectors[:, row_inds[0]] = 0.
    # vectors[:, row_inds[1]] = 0.
    vectors[:, row_inds[2]] = det_px_vert_size
    # column step (vX, vY, vZ if not swap_uv else uX, uY, uZ)
    vectors[:, col_inds[0]] = det_px_horz_size * cos_angles
    vectors[:, col_inds[1]] = det_px_horz_size * sin_angles
    # vectors[:, col_inds[2]] = 0.

    return vectors

def get_cone_beam_vec_proj_geom(src_radius, det_radius, angles, det_row_count, det_col_count, det_px_vert_size, det_px_horz_size=None, swap_uv=False, angle_offset=0., reverse_angles=False):
    """
    Create a cone-beam projection geometry of ASTRA type `'cone_vec'`
    (with the rotation axis being orthogonal to the xy plane and in the center
    of the volume).

    See the documentation of :func:`get_cone_beam_proj_geom` for the
    parameters not documented below.

    Parameters
    ----------
    swap_uv : bool, optional
        If `True`, swap detector rows and columns, resulting in projection data
        with dimension order `'uav'`.
        The default is `False`, resulting in dimension order `'vau'`.
    """
    if det_px_horz_size is None:
        det_px_horz_size = get_default_det_px_horz_size_cone_beam(src_radius, det_radius, det_col_count)

    if swap_uv:
        det_row_count, det_col_count = det_col_count, det_row_count

    vectors = get_cone_beam_vectors(src_radius, det_radius, angles, det_px_vert_size, det_px_horz_size=det_px_horz_size, swap_uv=swap_uv, angle_offset=angle_offset, reverse_angles=reverse_angles)

    proj_geom = astra.create_proj_geom('cone_vec', det_row_count, det_col_count, vectors)
    return proj_geom


# ray transform
class ASTRARayTrafoConeBeam(RayTrafo):
    def __init__(self, im_shape, src_radius, det_radius, angles, det_row_count, det_col_count, im_px_size=None, det_px_size=(None, None), vec=True, vec_has_swapped_uv=False, vol_indexing='zyx', proj_indexing='auv'):
        """
        Cone-beam ray transform
        (with the rotation axis being orthogonal to the xy plane and in the center
        of the volume).

        Parameters
        ----------
        im_shape : tuple of int
            Volume shape, a permutation of `(n_z, n_y, n_x)` depending on
            :param:`vol_indexing`.
        src_radius : float
            Source radius (distance between source and origin).
        det_radius : float
            Detector radius (distance between detector and origin).
        angles : array-like
            Rotation angles at which to compute projections.
        det_row_count : int
            Number of detector pixel rows.
        det_col_count : int
            Number of detector pixel columns.
        im_px_size : float or 3-tuple of float, optional
            Image voxel size; if a 3-tuple is specified, the dimension order
            should be as specified by :param:`vol_indexing` (i.e., matching the
            dimension order of :param:`im_shape` and :param:`x` passed to
            :meth:`apply`).
            If `None` or a scalar, the tuple is determined by
            :func:`UtilsASTRA.get_default_im_shape_and_px_size_3d`.
        det_px_size : 2-tuple of float, optional
            Detector pixel size (row and column spacing):
            `(det_px_vert_size, det_px_horz_size)`. If either value is `None`,
            it is determined by :func:`get_default_det_px_vert_size_cone_beam` or
            :func:`get_default_det_px_horz_size_cone_beam`, respectively, with
            `xy_diagonal` being calculated from ``x.shape`` and :param:`im_px_size`.
            The default is `(None, None)`.
        vec : bool or array, optional
            If a bool, whether to use the `'cone_vec'` or the `'cone'` projection
            routine; if an array, specifies the vectors for a custom `'cone_vec'`
            geometry.
            The default is `True`, i.e. the standard geometry via `'cone_vec'`.
        vec_has_swapped_uv : bool, optional
            Whether the columns ``vec[:, 6:9]`` and ``vec[:, 9:12]`` are swapped in
            `vec` if it is an array. The default is `False`.
        vol_indexing : str, optional
            Volume dimension order, must be a permutation of `'zyx'` (the default).
            If the z dimension is not the first one, it will be moved there
            explicitly, but swapping x and y dimensions is implemented by adapting
            the angles internally and flipping the obtained projections along the
            horizontal detector axis (cf. ODL's ASTRA backend).
        proj_indexing : str, optional
            Projection dimension order to which the output should be converted.
            Must be a permutation of `'auv'` (the default). `'a'` denotes angles,
            `'u'` denotes detector columns, and `'v'` denotes detector rows.
        """
        self.im_shape = im_shape
        self.angles = angles
        self.det_row_count = det_row_count
        self.det_col_count = det_col_count

        self.num_angles = len(self.angles)

        if np.isscalar(vec):
            vec = bool(vec)
        else:
            vec = np.asarray(vec)

        self.is_default_geometry = isinstance(vec, bool)

        if self.is_default_geometry:
            self._swap_uv = vec  # for better memory layout (see comment in odl.astra_setup.astra_conebeam_3d_geom_to_vec); only supported with cone_vec
        else:
            self._swap_uv = vec_has_swapped_uv  # use the given vectors as they are, if swapping is desired the user should already provide such `vec` for efficiency

        self.astra_proj_shape = (
                (self.det_col_count, self.num_angles, self.det_row_count) if self._swap_uv else
                (self.det_row_count, self.num_angles, self.det_col_count))

        self.vol_indexing = vol_indexing

        if self.is_default_geometry:  # angle adaptation trick for implicit xy transposition is applicable
            ## move z to dim 0
            # determine working vol_indexing order
            self._input_z_dim = self.vol_indexing.index('z')
            working_vol_indexing = 'z' + self.vol_indexing[:self._input_z_dim] + self.vol_indexing[self._input_z_dim+1:]
            assert working_vol_indexing in ['zxy', 'zyx']
        else:
            ## convert volume data to ASTRA's native zyx order
            working_vol_indexing = 'zyx'

        if self.is_default_geometry:
            # use angles to adapt to different image coordinates (avoid transposing the array)
            if working_vol_indexing[1:] == 'xy':
                # use case A) described in get_cone_beam_proj_geom()
                self._angle_offset = -1.5 * np.pi
                self._reverse_angles = True
                self._flip_det_horz = True
            else:  # 'yx':
                # use case B) described in get_cone_beam_proj_geom()
                self._angle_offset = 0.
                self._reverse_angles = False
                self._flip_det_horz = False
        else:  # custom geometry
            self._flip_det_horz = False

        self.proj_indexing = proj_indexing

        self.proj_shape = [
                self.astra_proj_shape[('uav' if self._swap_uv else 'vau').index(d)]
                for d in self.proj_indexing]

        # reorder shape and px sizes from input vol_indexing order to working_vol_indexing
        self.astra_im_shape = [
                self.im_shape[self.vol_indexing.index(d)]
                for d in working_vol_indexing]
        if im_px_size is None or np.isscalar(im_px_size):
            astra_im_px_size = im_px_size
        else:
            astra_im_px_size = [
                    im_px_size[self.vol_indexing.index(d)]
                    for d in working_vol_indexing]
        _, astra_im_px_size = get_default_im_shape_and_px_size_3d(self.astra_im_shape, im_px_size=astra_im_px_size)
        # create volume geometry
        self.vol_geom = get_3d_vol_geom(self.astra_im_shape, im_px_size=astra_im_px_size)

        det_px_vert_size, det_px_horz_size = det_px_size
        xy_diagonal = get_xy_diagonal(self.astra_im_shape, astra_im_px_size)
        if det_px_vert_size is None:
            det_px_vert_size = get_default_det_px_vert_size_cone_beam(src_radius, det_radius, astra_im_px_size[0], xy_diagonal=xy_diagonal)
        if det_px_horz_size is None:
            det_px_horz_size = get_default_det_px_horz_size_cone_beam(src_radius, det_radius, self.det_col_count, xy_diagonal=xy_diagonal)
        # create projection geometry
        if vec is True:
            self.proj_geom = get_cone_beam_vec_proj_geom(src_radius, det_radius, self.angles, self.det_row_count, self.det_col_count, det_px_vert_size=det_px_vert_size, det_px_horz_size=det_px_horz_size, swap_uv=self._swap_uv, angle_offset=self._angle_offset, reverse_angles=self._reverse_angles)
        elif vec is False:
            self.proj_geom = get_cone_beam_proj_geom(src_radius, det_radius, self.angles, self.det_row_count, self.det_col_count, det_px_vert_size=det_px_vert_size, det_px_horz_size=det_px_horz_size, angle_offset=self._angle_offset, reverse_angles=self._reverse_angles)
        else:  # vec is a manually specified array
            self.proj_geom = astra.create_proj_geom('cone_vec', self.astra_proj_shape[0], self.astra_proj_shape[2], vec)

        if TORCH_AVAILABLE:
            self.create_torch_modules()

    def create_torch_modules(self):
        self.fp_module = TorchDirectASTRARayTrafoModule(
                self.vol_geom, self.proj_geom, adjoint=False)
        self.bp_module = TorchDirectASTRARayTrafoModule(
                self.vol_geom, self.proj_geom, adjoint=True)

    def apply(self, x):
        """
        Parameters
        ----------
        x : array-like
            Volume data, shape is a permutation of `(n_z, n_y, n_x)` depending on
            :attr:`vol_indexing`.

        Returns
        -------
        y : :class:`np.ndarray`
            Projection data, shape is a permutation of
            `(angles, det_cols, det_rows)` depending on :attr:`proj_indexing`.
        """
        ## convert volume data to ASTRA's native zyx order
        if self.is_default_geometry:  # use angle adaptation trick for implicit xy transposition
            # move z dim in volume data, see working_vol_indexing in __init__()
            x = np.ascontiguousarray(np.moveaxis(x, self._input_z_dim, 0), dtype=np.float32)
        else:
            # convert to zyx explicitly
            vol_transpose_axes = [self.vol_indexing.index(d) for d in 'zyx']
            x = np.ascontiguousarray(np.transpose(x, axes=vol_transpose_axes), dtype=np.float32)

        projs = np.zeros(self.astra_proj_shape, dtype=np.float32)

        # run forward projection
        with AstraLinkedFP(self.proj_geom, self.vol_geom, x, projs) as alg_id:
            astra.algorithm.run(alg_id)

        # determine tranposition to obtain requested coordinates from coordinates produced by ASTRA
        proj_transpose_axes = [('uav' if self._swap_uv else 'vau').index(d) for d in self.proj_indexing]

        projs = np.transpose(projs, axes=proj_transpose_axes)
        if self._flip_det_horz:
            projs = np.flip(projs, axis=self.proj_indexing.index('u'))

        return projs

    def apply_adjoint(self, y):
        """
        Parameters
        ----------
        y : array-like
            Projection data, shape is a permutation of
            `(angles, det_cols, det_rows)` depending on :attr:`proj_indexing`.

        Returns
        -------
        x : :class:`np.ndarray`
            Volume data, shape is a permutation of `(n_z, n_y, n_x)` depending on
            :attr:`vol_indexing`.
        """
        projs = y
        if self._flip_det_horz:
            projs = np.flip(projs, axis=self.proj_indexing.index('u'))

        # determine tranposition to obtain ASTRA coordinates from provided coordinates
        proj_transpose_axes = [self.proj_indexing.index(d) for d in ('uav' if self._swap_uv else 'vau')]

        projs = np.ascontiguousarray(np.transpose(projs, axes=proj_transpose_axes), dtype=np.float32)

        x = np.zeros(self.astra_im_shape, dtype=np.float32)

        # run back-projection
        with AstraLinkedBP(self.proj_geom, self.vol_geom, x, projs) as alg_id:
            astra.algorithm.run(alg_id)

        ## convert volume data to requested vol_indexing order
        if self.is_default_geometry:  # use angle adaptation trick for implicit xy transposition
            # move z dim in volume data, see working_vol_indexing in __init__()
            # (working_vol_indexing in ['zxy', 'zyx'])
            x = np.moveaxis(x, 0, self._input_z_dim)
        else:
            # convert from zyx to vol_indexing explicitly
            # (working_vol_indexing == 'zyx')
            vol_transpose_axes = ['zyx'.index(d) for d in self.vol_indexing]
            x = np.transpose(x, axes=vol_transpose_axes)

        return x

    def apply_torch(self, x):
        """
        Forward project a torch tensor. If `x` requires gradients, they are
        provided via the back-projection (not the exact discrete adjoint).

        Parameters
        ----------
        x : :class:`torch.Tensor`
            Volume data, shape is a permutation of `(n_z, n_y, n_x)` depending
            on :attr:`vol_indexing` prepended by an arbitrary number of leading
            dimensions.

        Returns
        -------
        y : :class:`torch.Tensor`
            Projection data, shape is a permutation of
            `(angles, det_cols, det_rows)` depending on :attr:`proj_indexing`
            prepended by the same leading dimensions as `x`.
        """
        leading_ndim = x.ndim - 3
        ## convert volume data to ASTRA's native zyx order
        if self.is_default_geometry:  # use angle adaptation trick for implicit xy transposition
            # move z dim in volume data, see working_vol_indexing in __init__()
            x = torch.moveaxis(
                    x, leading_ndim + self._input_z_dim, leading_ndim).float()
        else:
            # convert to zyx explicitly
            vol_transpose_axes = list(range(leading_ndim)) + [
                    leading_ndim + self.vol_indexing.index(d) for d in 'zyx']
            x = torch.permute(x, dims=vol_transpose_axes).float()

        # run forward projection
        projs = self.fp_module(x)

        # determine tranposition to obtain requested coordinates from coordinates produced by ASTRA
        proj_transpose_axes = list(range(leading_ndim)) + [
                leading_ndim + ('uav' if self._swap_uv else 'vau').index(d)
                for d in self.proj_indexing]

        projs = torch.permute(projs, dims=proj_transpose_axes)
        if self._flip_det_horz:
            projs = torch.flip(projs, dims=(leading_ndim + self.proj_indexing.index('u'),))
        return projs

    def apply_adjoint_torch(self, y):
        """
        Back-project a torch tensor. If `y` requires gradients, they are
        provided via the forward projection (not the exact discrete adjoint).

        Parameters
        ----------
        y : :class:`torch.Tensor`
            Projection data, shape is a permutation of
            `(angles, det_cols, det_rows)` depending on :attr:`proj_indexing`
            prepended by an arbitrary number of leading dimensions.

        Returns
        -------
        x : :class:`torch.Tensor`
            Volume data, shape is a permutation of `(n_z, n_y, n_x)` depending
            on :attr:`vol_indexing` prepended by the same leading dimensions as
            `y`.
        """
        projs = y
        leading_ndim = projs.ndim - 3

        if self._flip_det_horz:
            projs = torch.flip(projs, dims=(leading_ndim + self.proj_indexing.index('u'),))

        # determine tranposition to obtain ASTRA coordinates from provided coordinates
        proj_transpose_axes = list(range(leading_ndim)) + [
                leading_ndim + self.proj_indexing.index(d)
                for d in ('uav' if self._swap_uv else 'vau')]

        projs = torch.permute(projs, dims=proj_transpose_axes).float()

        # run back-projection
        x = self.bp_module(projs)

        ## convert volume data to requested vol_indexing order
        if self.is_default_geometry:  # use angle adaptation trick for implicit xy transposition
            # move z dim in volume data, see working_vol_indexing in __init__()
            # (working_vol_indexing in ['zxy', 'zyx'])
            x = torch.moveaxis(
                    x, leading_ndim, leading_ndim + self._input_z_dim)
        else:
            # convert from zyx to vol_indexing explicitly
            # (working_vol_indexing == 'zyx')
            vol_transpose_axes = list(range(leading_ndim)) + [
                    leading_ndim + 'zyx'.index(d) for d in self.vol_indexing]
            x = torch.permute(x, dims=vol_transpose_axes)

        return x

def get_deviating_cone_beam_vectors(vec, src_shifts_xyz=None, det_shifts_xyz=None, det_shifts_uv=None, det_shifts_src_to_det=None, swapped_uv=False):
    """
    Return modified vectors for an ASTRA `'cone_vec'` geometry.

    Parameters
    ----------
    src_shifts_xyz : array-like, optional
        Source position shift in xyz (volume) coordinates.
        Must broadcast to shape ``(len(vec), 3)``.
    det_shifts_xyz : array-like, optional
        Detector position shift in xyz (volume) coordinates.
        Must broadcast to shape ``(len(vec), 3)``.
    det_shifts_uv : array-like, optional
        Detector position shift in uv (detector axis) coordinates.
        The first coordinate specifies the shift in `u` axis direction; the
        second coordinate specifies the shift in `v` axis direction.
        Must broadcast to shape ``(len(vec), 2)``.
    det_shifts_src_to_det : array-like, optional
        Detector position shift in source-to-detector direction.
        Must broadcast to shape ``(len(vec),)``.
    swapped_uv : bool, optional
        If `True`, detector rows and columns in `vec` (if specified) are
        expected to be swapped and the returned deviating vectors will also be
        swapped.
    """
    deviating_vec = np.copy(vec)
    if swapped_uv:
        u_slice, v_slice = slice(9, 12), slice(6, 9)
    else:
        u_slice, v_slice = slice(6, 9), slice(9, 12)

    if src_shifts_xyz is not None:
        deviating_vec[:, 0:3] += src_shifts_xyz
    if det_shifts_xyz is not None:
        deviating_vec[:, 3:6] += det_shifts_xyz
    if det_shifts_uv is not None:
        deviating_vec[:, 3:6] += np.asarray(det_shifts_uv)[:, 0] * (vec[:, u_slice] / np.sqrt(np.sum(vec[:, u_slice]**2, axis=1)))
        deviating_vec[:, 3:6] += np.asarray(det_shifts_uv)[:, 1] * (vec[:, v_slice] / np.sqrt(np.sum(vec[:, v_slice]**2, axis=1)))
    if det_shifts_src_to_det is not None:
        deviating_vec[:, 3:6] += np.asarray(det_shifts_src_to_det) * (vec[:, 3:6] - vec[:, 0:3])

    return deviating_vec

def get_deviating_astra_ray_trafo_cone_beam(
        im_shape, src_radius, det_radius, angles, det_row_count, det_col_count,
        deviation_kwargs=None, im_px_size=None, det_px_size=(None, None), vec=None,
        swapped_uv=False, vol_indexing='zyx', proj_indexing='auv'):
    """
    See the documentation of :meth:`ASTRARayTrafoConeBeam.__init__` for the
    parameters not documented below.

    Parameters
    ----------
    vec : array, optional
        Custom vectors for the non-deviating `'cone_vec'` projection geometry.
        If `None`, the default geometry is used as specified by the other
        parameters.
    deviation_kwargs : dict, optional
        Parameters passed to :func:`get_deviating_cone_beam_vectors`, specifying
        the deviation.
    swapped_uv : bool, optional
        If `True`, detector rows and columns in `vec` (if specified) are
        expected to be swapped and :class:`ASTRARayTrafoConeBeam` will be
        created with such vectors (``vec_has_swapped_uv=True``).
    """
    if vec is None:
        # reorder shape and px sizes from input vol_indexing order to native ASTRA zyx order
        astra_im_shape = [im_shape[vol_indexing.index(d)] for d in 'zyx']
        if im_px_size is None or np.isscalar(im_px_size):
            astra_im_px_size = im_px_size
        else:
            astra_im_px_size = [im_px_size[vol_indexing.index(d)] for d in 'zyx']
        _, astra_im_px_size = get_default_im_shape_and_px_size_3d(astra_im_shape, im_px_size=astra_im_px_size)

        det_px_vert_size, det_px_horz_size = det_px_size
        xy_diagonal = get_xy_diagonal(astra_im_shape, astra_im_px_size)
        if det_px_vert_size is None:
            det_px_vert_size = get_default_det_px_vert_size_cone_beam(src_radius, det_radius, astra_im_px_size[0], xy_diagonal=xy_diagonal)
        if det_px_horz_size is None:
            det_px_horz_size = get_default_det_px_horz_size_cone_beam(src_radius, det_radius, det_col_count, xy_diagonal=xy_diagonal)

        vec = get_cone_beam_vectors(src_radius, det_radius, angles, det_px_vert_size, det_px_horz_size=det_px_horz_size, swap_uv=swapped_uv, angle_offset=0., reverse_angles=False)

    if deviation_kwargs is None:
        deviation_kwargs = {}

    deviating_vec = get_deviating_cone_beam_vectors(vec, **deviation_kwargs, swapped_uv=swapped_uv)

    deviating_ray_trafo = ASTRARayTrafoConeBeam(
            im_shape, src_radius, det_radius, angles,
            det_row_count, det_col_count, im_px_size=im_px_size,
            det_px_size=det_px_size, vec=deviating_vec,
            vec_has_swapped_uv=swapped_uv, vol_indexing=vol_indexing,
            proj_indexing=proj_indexing)

    return deviating_ray_trafo
