import numpy as np
import astra
import torch
from torch import nn
import tomosipo as ts
from tomosipo.torch_support import to_autograd
from .UtilsASTRA import AstraLinkedFP, AstraLinkedBP

# clone from tomosipo.torch_support, but with
# @torch.cuda.amp.custom_fwd and @torch.cuda.amp.custom_bwd in order to enforce
# float32 within amp autocast
class OperatorFunctionFloat32(torch.autograd.Function):
    @staticmethod
    @torch.cuda.amp.custom_fwd(cast_inputs=torch.float32)
    def forward(ctx, input, operator):
        # if input.requires_grad:  # seems to be always False if custom_fwd is
        # used, even if grad of the original input is required
        ctx.operator = operator
        assert (
            input.ndim == 4
        ), "Autograd operator expects a 4-dimensional input (3+1 for Batch dimension). "

        B, C, H, W = input.shape
        out = input.new_empty(B, *operator.range_shape)

        for i in range(B):
            operator(input[i], out=out[i])

        return out

    @staticmethod
    @torch.cuda.amp.custom_bwd
    def backward(ctx, grad_output):
        operator = ctx.operator

        B, C, H, W = grad_output.shape
        grad_input = grad_output.new_empty(B, *operator.domain_shape)

        for i in range(B):
            operator.T(grad_output[i], out=grad_input[i])

        # do not return gradient for operator
        return grad_input, None

# clone from tomosipo.torch_support, but using OperatorFunctionFloat32
def to_autograd_float32(operator):
    def f(x):
        return OperatorFunctionFloat32.apply(x, operator)

    return f

class TorchDirectASTRARayTrafoModule(nn.Module):
    """
    Module applying ASTRA direct 3D forward- or back-projections via tomosipo.
    Gradients will be computed via the discretization of the analytical adjoint,
    which might deviate slightly from the adjoint of the discrete forward pass.
    """
    def __init__(self, vol_geom, proj_geom, adjoint=False):
        """
        Parameters
        ----------
        vol_geom : dict
            ASTRA 3D volume geometry
        proj_geom : dict
            ASTRA 3D projection geometry
        adjoint : bool, optional
            If `False` (the default), compute the forward-projection in
            :meth:`forward`; if `True`, compute the back-projection instead.
        """
        super().__init__()
        self.vol_geom = vol_geom
        self.proj_geom = proj_geom
        self.adjoint = adjoint

        ts_operator_fp = ts.operator(
                ts.from_astra(vol_geom), ts.from_astra(proj_geom))
        self.ts_operator = ts_operator_fp.T if self.adjoint else ts_operator_fp
        self.forward_fun = to_autograd_float32(self.ts_operator)

    def forward(self, inp):
        """
        Apply the forward- or back-projection.

        Parameters
        ----------
        inp : :class:`torch.Tensor`
            For forward-projection (:attr:`adjoint` is `False`):
                    shape ``... x Z x Y x X``;
            for backward-projection (:attr:`adjoint` is `True`):
                    shape ``... x det_rows x angles x det_cols``.
            Any leading dimensions are treated as batch dimensions.
        """
        orig_batch_dims = inp.shape[:-3]
        inp = inp.view(-1, *inp.shape[-3:])

        out = self.forward_fun(inp)

        out = out.view(*orig_batch_dims, *out.shape[-3:])
        return out

# based on
# https://github.com/odlgroup/odl/blob/25ec783954a85c2294ad5b76414f8c7c3cd2785d/odl/contrib/torch/operator.py#L33
class NumpyFunction(torch.autograd.Function):
    """
    Wraps a forward and backward function pair operating on numpy arrays as a
    torch autograd function.
    """
    # pylint: disable=abstract-method

    @staticmethod
    def forward(ctx, x, forward_fun, backward_fun):  # pylint: disable=arguments-differ
        ctx.forward_fun = forward_fun
        ctx.backward_fun = backward_fun

        x_np = x.detach().cpu().numpy()
        y_np = np.stack([ctx.forward_fun(x_np_i) for x_np_i in x_np])
        # y_np = ctx.forward_fun(x_np)
        y = torch.from_numpy(y_np).to(x.device)
        return y

    @staticmethod
    def backward(ctx, y):  # pylint: disable=arguments-differ
        y_np = y.detach().cpu().numpy()
        x_np = np.stack([ctx.backward_fun(y_np_i) for y_np_i in y_np])
        # x_np = ctx.backward_fun(y_np)
        x = torch.from_numpy(x_np).to(y.device)
        return x, None, None

class TorchLinkedASTRARayTrafoModule(nn.Module):
    """
    Module applying ASTRA forward- or back-projections on linked arrays.
    In contrast to :class:`TorchDirectASTRARayTrafoModule`, this module can also
    be used for 2D data.
    Gradients will be computed via the discretization of the analytical adjoint,
    which might deviate slightly from the adjoint of the discrete forward pass.
    """
    def __init__(self, vol_geom, proj_geom, adjoint=False):
        """
        Parameters
        ----------
        vol_geom : dict
            ASTRA volume geometry
        proj_geom : dict
            ASTRA projection geometry
        adjoint : bool, optional
            If `False` (the default), compute the forward-projection in
            :meth:`forward`; if `True`, compute the back-projection instead.
        """
        super().__init__()
        if proj_geom['type'] in ['parallel', 'parallel_vec', 'fanflat', 'fanflat_vec']:
            self.is_3d = False
            self.vol_shape = [
                    vol_geom['GridRowCount'],
                    vol_geom['GridColCount']]
            num_angles = len(proj_geom[
                    'Vectors' if '_vec' in proj_geom['type']
                    else 'ProjectionAngles'])
            self.proj_shape = [
                    num_angles,
                    proj_geom['DetectorCount']]
        elif proj_geom['type'] in ['parallel3d', 'parallel3d_vec', 'cone', 'cone_vec']:
            self.is_3d = True
            self.vol_shape = [
                    vol_geom['GridRowCount'],
                    vol_geom['GridColCount'],
                    vol_geom['GridSliceCount']]
            num_angles = len(proj_geom[
                    'Vectors' if '_vec' in proj_geom['type']
                    else 'ProjectionAngles'])
            self.proj_shape = [
                    proj_geom['DetectorRowCount'],
                    num_angles,
                    proj_geom['DetectorColCount']]
        else:
            raise NotImplementedError
        self.vol_geom = vol_geom
        self.proj_geom = proj_geom
        self.adjoint = adjoint

    def _astra_fp(self, x):
        x = np.ascontiguousarray(x, dtype=np.float32)
        projs = np.zeros(self.proj_shape, dtype=np.float32)

        with AstraLinkedFP(self.proj_geom, self.vol_geom, x, projs) as alg_id:
            astra.algorithm.run(alg_id)

        return projs

    def _astra_bp(self, y):
        projs = y
        projs = np.ascontiguousarray(projs, dtype=np.float32)
        x = np.zeros(self.vol_shape, dtype=np.float32)

        with AstraLinkedBP(self.proj_geom, self.vol_geom, x, projs) as alg_id:
            astra.algorithm.run(alg_id)

        return x

    def forward(self, inp):
        """
        Apply the forward- or back-projection.

        Parameters
        ----------
        inp : :class:`torch.Tensor`
            For forward-projection (:attr:`adjoint` is `False`):
                    shape ``... x [Z x] Y x X``;
            for backward-projection (:attr:`adjoint` is `True`):
                    shape ``... x [det_rows x] angles x det_cols``.
            Any leading dimensions are treated as batch dimensions.
        """
        ndim = 3 if self.is_3d else 2
        orig_batch_dims = inp.shape[:-ndim]
        inp = inp.view(-1, *inp.shape[-ndim:])

        if self.adjoint:
            forward_fun, backward_fun = self._astra_bp, self._astra_fp
        else:
            forward_fun, backward_fun = self._astra_fp, self._astra_bp

        out = NumpyFunction.apply(inp, forward_fun, backward_fun)

        out = out.view(*orig_batch_dims, *out.shape[-ndim:])
        return out
