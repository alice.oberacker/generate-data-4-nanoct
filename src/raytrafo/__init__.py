from .MatrixRayTrafo import MatrixRayTrafo
try:
    import torch
except ImportError:
    pass
else:
    from .MatrixRayTrafoTorch import get_matrix_ray_trafo_module
try:
    import odl
except ImportError:
    pass
# else:
#     from .ParallelODL import get_ray_trafo_parallel_2d_odl, get_ray_trafo_matrix_parallel_2d_odl
