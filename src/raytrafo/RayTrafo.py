
class _RayTrafoAdjoint:
    def __init__(self, ray_trafo):
        self.ray_trafo = ray_trafo

    def __call__(self, y):
        return self.ray_trafo.apply_adjoint(y)

# Base for ray transforms with an interface similar to odl.tomo.RayTransform
class RayTrafo:
    def apply(self, x):
        """
        Apply the forward projection.

        Parameters
        ----------
        x : array-like
            Image of attenuation.
            Implementations should support a 2D/3D array for 2D/3D geometries;
            a flattened version can be supported additionally.

        Returns
        -------
        y : :class:`np.ndarray`
            Forward projection, the shape depends on the implementation.
        """
        raise NotImplementedError

    def apply_adjoint(self, y):
        """
        Apply the adjoint operation (commonly implemented as a back-projection;
        for an accurately matching adjoint of the discrete forward projection
        operation :meth:`apply`, one might consider assembling the matrix
        describing :meth:`apply` and multiply with the transposed matrix in this
        function).

        Parameters
        ----------
        y : array-like
            Projection values.
            Implementations should support the shape of the array returned by
            :meth:`apply`; a flattened version can be supported additionally.

        Returns
        -------
        x : :class:`np.ndarray`
            The image (back-projection) as a 2D/3D array for 2D/3D geometries,
            respectively.
        """
        raise NotImplementedError

    @property
    def adjoint(self):
        return _RayTrafoAdjoint(self)

    def __call__(self, x):
        return self.apply(x)
