from functools import partial
import numpy as np
import scipy.sparse
import astra
from tqdm import tqdm

class AstraLinkedProjection:
    def __init__(self, proj_geom, vol_geom, vol_array, sinogram_array, direction='FP'):
        assert direction in ['FP', 'BP']
        if proj_geom['type'] in ['parallel', 'parallel_vec', 'fanflat', 'fanflat_vec']:
            ALG_NAME = '{}_CUDA'.format(direction)
            astra_data_module = astra.data2d
        elif proj_geom['type'] in ['parallel3d', 'parallel3d_vec', 'cone', 'cone_vec']:
            ALG_NAME = '{}3D_CUDA'.format(direction)
            astra_data_module = astra.data3d
        else:
            raise NotImplementedError

        vol_id = astra_data_module.link('-vol', vol_geom, vol_array)
        sino_id = astra_data_module.link('-sino', proj_geom, sinogram_array)
        cfg = astra.astra_dict(ALG_NAME)
        if direction == 'FP':
            cfg['VolumeDataId'] = vol_id
        elif direction == 'BP':
            cfg['ReconstructionDataId'] = vol_id
        cfg['ProjectionDataId'] = sino_id
        self.alg_id = astra.algorithm.create(cfg)

    def __enter__(self):
        return self.alg_id

    def __exit__(self, type, value, traceback):
        astra.algorithm.delete(self.alg_id)


class AstraLinkedFP(AstraLinkedProjection):
    def __init__(self, proj_geom, vol_geom, vol_array, sinogram_array):
        super().__init__(proj_geom, vol_geom, vol_array, sinogram_array, direction='FP')


class AstraLinkedBP(AstraLinkedProjection):
    def __init__(self, proj_geom, vol_geom, vol_array, sinogram_array):
        super().__init__(proj_geom, vol_geom, vol_array, sinogram_array, direction='BP')


# dynamic default parameters

def get_default_im_shape_and_px_size(ndim, im_shape, im_px_size=None):
    if np.isscalar(im_shape):
        im_shape = (im_shape,) * ndim
    if im_px_size is None:
        im_px_size = 2. / max(im_shape)
    if np.isscalar(im_px_size):
        im_px_size = (im_px_size,) * ndim
    return im_shape, im_px_size

get_default_im_shape_and_px_size_2d = partial(get_default_im_shape_and_px_size, 2)
get_default_im_shape_and_px_size_3d = partial(get_default_im_shape_and_px_size, 3)


# utility function

def get_xy_diagonal(im_shape_yx, im_px_size_yx):
    """
    Return the diagonal size of the volume in xy-plane.

    Parameters
    ----------
    im_shape_yx : 2- or 3-tuple of int
        Shape of the image (in pixels): `(n_y, n_x)` or `(n_z, n_y, n_x)`.
        If a 3-tuple is specified, the value `n_z` will be ignored.
    im_px_size_yx : 2- or 3-tuple of float
        Image voxel size in (z)yx order.
        If a 3-tuple is specified, the value `im_px_size_yx[0]` will be ignored.
    """
    if len(im_shape_yx) == 3:
        im_shape_yx = im_shape_yx[1:]
    if len(im_px_size_yx) == 3:
        im_px_size_yx = im_px_size_yx[1:]
    xy_diagonal = np.sqrt((im_shape_yx[0] * im_px_size_yx[0])**2 + (im_shape_yx[1] * im_px_size_yx[1])**2)
    return xy_diagonal


# volume geometries (ASTRA dicts)

def get_2d_vol_geom(im_shape, im_px_size=None):
    im_shape, im_px_size = get_default_im_shape_and_px_size_2d(im_shape, im_px_size=im_px_size)

    vol_geom = astra.create_vol_geom(*im_shape)  # (y, x); (rows, columns)
    vol_geom['option']['WindowMinX'] = vol_geom['option']['WindowMinX'] * im_px_size[1]
    vol_geom['option']['WindowMaxX'] = vol_geom['option']['WindowMaxX'] * im_px_size[1]
    vol_geom['option']['WindowMinY'] = vol_geom['option']['WindowMinY'] * im_px_size[0]
    vol_geom['option']['WindowMaxY'] = vol_geom['option']['WindowMaxY'] * im_px_size[0]
    return vol_geom

def get_3d_vol_geom(im_shape, im_px_size=None):
    im_shape, im_px_size = get_default_im_shape_and_px_size_3d(im_shape, im_px_size=im_px_size)
    # image coordinates: (z, y, x)

    vol_geom = astra.create_vol_geom(im_shape[1], im_shape[2], im_shape[0])  # (y, x, z); (rows, columns, slices)
    vol_geom['option']['WindowMinX'] = vol_geom['option']['WindowMinX'] * im_px_size[2]
    vol_geom['option']['WindowMaxX'] = vol_geom['option']['WindowMaxX'] * im_px_size[2]
    vol_geom['option']['WindowMinY'] = vol_geom['option']['WindowMinY'] * im_px_size[1]
    vol_geom['option']['WindowMaxY'] = vol_geom['option']['WindowMaxY'] * im_px_size[1]
    vol_geom['option']['WindowMinZ'] = vol_geom['option']['WindowMinZ'] * im_px_size[0]
    vol_geom['option']['WindowMaxZ'] = vol_geom['option']['WindowMaxZ'] * im_px_size[0]
    return vol_geom
