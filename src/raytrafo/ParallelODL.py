import numpy as np
import odl

def get_ray_trafo_parallel_2d_odl(im_shape, num_angles=None, det_shape=None, min_pt=(-1., -1.), max_pt=(1., 1.), dtype='float32', impl='astra_cuda'):
    if np.isscalar(im_shape):
        im_shape = [im_shape, im_shape]
    im_space = odl.uniform_discr(
            min_pt=min_pt,
            max_pt=max_pt,
            shape=im_shape, dtype=dtype)
    geometry = odl.tomo.parallel_beam_geometry(im_space, num_angles=num_angles, det_shape=det_shape)
    ray_trafo = odl.tomo.RayTransform(im_space, geometry, impl=impl)
    return ray_trafo

def get_ray_trafo_parallel_3d_odl(im_shape, num_angles=None, det_shape=None, min_pt=(-1., -1., -1.), max_pt=(1., 1., 1.), dtype='float32', impl='astra_cuda'):
    if np.isscalar(im_shape):
        im_shape = [im_shape, im_shape, im_shape]
    im_space = odl.uniform_discr(
            min_pt=min_pt,
            max_pt=max_pt,
            shape=im_shape, dtype=dtype)
    geometry = odl.tomo.parallel_beam_geometry(im_space, num_angles=num_angles, det_shape=det_shape)
    ray_trafo = odl.tomo.RayTransform(im_space, geometry, impl=impl)
    return ray_trafo

def get_ray_trafo_matrix_parallel_2d_odl(im_shape, num_angles=None, det_shape=None, dtype='float32', impl='astra_cuda', flatten=True, order='C'):
    ray_trafo = get_ray_trafo_parallel_2d_odl(im_shape, num_angles=num_angles, det_shape=det_shape, dtype=dtype, impl=impl)
    matrix = odl.operator.matrix_representation(ray_trafo)
    if flatten:
        matrix = matrix.reshape(np.prod(ray_trafo.range.shape), np.prod(ray_trafo.domain.shape), order=order)
    return matrix
