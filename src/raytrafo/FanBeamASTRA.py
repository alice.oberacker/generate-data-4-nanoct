import numpy as np
import scipy.sparse
import astra
from tqdm import tqdm
try:
    import torch
    TORCH_AVAILABLE = True
except ImportError:
    TORCH_AVAILABLE = False

from .UtilsASTRA import AstraLinkedFP, AstraLinkedBP, get_2d_vol_geom, get_default_im_shape_and_px_size_2d, get_xy_diagonal
from .RayTrafo import RayTrafo
if TORCH_AVAILABLE:
    from .UtilsTorchASTRA import TorchLinkedASTRARayTrafoModule

# create aliases for functions from ConeBeamASTRA that support fan beam as well
from .ConeBeamASTRA import (
        get_default_det_col_count_cone_beam as get_default_det_shape_fan_beam,
        get_angles_cone_beam as get_angles_fan_beam)


# dynamic default parameters

def get_default_det_px_size_fan_beam(src_radius, det_radius, det_shape, xy_diagonal=2.*np.sqrt(2.)):
    """
    Return default detector pixel (column) spacing spanning the maximum extent
    :param:`xy_diagonal` of the volume in the xy-plane, measured at the origin
    parallel to the detector.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    det_shape : int
        Number of detector pixels (columns) to distribute.
    xy_diagonal : float, optional
        Maximum extent of the volume in the xy-plane, measured at the origin
        parallel to the detector. The default is ``2. * sqrt(2.)``.
    """
    # adapted from odl.tomo.cone_beam_geometry()
    det_px_horz_size = (xy_diagonal * (src_radius + det_radius) / src_radius) / det_shape
    return det_px_horz_size

# projection geometries (ASTRA dicts)

def get_fan_beam_proj_geom(src_radius, det_radius, angles, det_shape, det_px_size=None, angle_offset=float(1.5*np.pi), reverse_angles=False):
    """
    Create a fan-beam projection geometry of ASTRA type `'fanflat'`
    (with the rotation axis being orthogonal to the xy plane and in the center
    of the volume).

    Regarding the different angle conventions in ODL and ASTRA, consider the
    following two use cases:

        A)  `'xy'` volume data should be projected without explicit
            transposition to ASTRA's native `'yx'` order (intentionally using
            ASTRA's `'fanflat'` geometry with swapped-order volume data).
            To obtain projections matching those from an ODL geometry with
            the same angle values ``angles == odl_geometry.angles``, use
            ``angle_offset=1.5*pi, reverse_angles=False`` (the default), and
            leave the obtained projections just as they are (which means they
            are flipped compared to ASTRA `'fanflat'`'s behaviour); note that
            the :class:`odl.tomo.RayTransform` is called on `'xy'` volume data.
        B)  `'yx'` volume data should be projected, simply following all
            conventions of the ASTRA `'fanflat'` geometry.
            To obtain a geometry matching case A), specify
            ``angle_offset=pi, reverse_angles=True`` and flip the obtained
            projections along the detector axis, assuming the same
            :param:`angles` for both cases.

    In both cases, to use angles like :func:`odl.tomo.cone_beam_geometry`, pass
    ``get_angles_fan_beam(..., use_angles_like_odl=True)`` as the
    :param:`angles` argument of this function.

    Parameters
    ----------
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    angles : array-like
        Rotation angles.
        Angles follow the convention of ASTRA's `'fanflat'` geometry.
    det_shape : int
        Number of detector pixels (columns).
    det_px_size : float, optional
        Detector pixel size (column spacing).
        If `None`, it is determined by
        :func:`get_default_det_px_size_fan_beam` assuming the default
        ``xy_diagonal=2.*np.sqrt(2.)``.
    angle_offset : float, optional
        Constant value to add to every rotation angle. The default is `0.`.
    reverse_angles : bool, optional
        If `True`, multiply each angle with `-1.`. The default is `False`.
    """
    if det_px_size is None:
        det_px_size = get_default_det_px_size_fan_beam(src_radius, det_radius, det_shape)

    angles = np.asarray(angles)
    num_angles = len(angles)

    if angle_offset:
        angles = angles + angle_offset
    if reverse_angles:
        angles = -angles

    proj_geom = astra.create_proj_geom('fanflat', det_px_size, det_shape, angles, src_radius, det_radius)
    return proj_geom

def get_fan_beam_vectors(src_radius, det_radius, angles, det_px_size, angle_offset=float(1.5*np.pi), reverse_angles=False):
    """
    Return vectors for an ASTRA `'fanflat_vec'` geometry (with the rotation axis
    being orthogonal to the xy plane and in the center of the volume).

    See the documentation of :func:`get_fan_beam_proj_geom` for the
    parameters not documented below.

    Parameters
    ----------
    det_px_size : float
        Horizontal detector pixel size (column spacing).
        This parameter is required (unlike for :func:`get_fan_beam_proj_geom`).
    """
    angles = np.asarray(angles)
    num_angles = len(angles)

    if angle_offset:
        angles = angles + angle_offset
    if reverse_angles:
        angles = -angles

    sin_angles = np.sin(angles)
    cos_angles = np.cos(angles)

    vectors = np.zeros((num_angles, 6))
    # rayX, rayY
    vectors[:, 0] = src_radius * sin_angles
    vectors[:, 1] = -src_radius * cos_angles
    # dX, dY
    vectors[:, 2] = -det_radius * sin_angles
    vectors[:, 3] = det_radius * cos_angles
    # step
    vectors[:, 4] = det_px_size * cos_angles
    vectors[:, 5] = det_px_size * sin_angles

    return vectors

def get_fan_beam_vec_proj_geom(src_radius, det_radius, angles, det_shape, det_px_size=None, angle_offset=float(1.5*np.pi), reverse_angles=False):
    """
    Create a fan-beam projection geometry of ASTRA type `'fanflat_vec'`
    (with the rotation axis being orthogonal to the xy plane and in the center
    of the volume).

    See the documentation of :func:`get_fan_beam_proj_geom` for the parameters.
    """
    if det_px_size is None:
        det_px_size = get_default_det_px_size_fan_beam(src_radius, det_radius, det_shape)

    vectors = get_fan_beam_vectors(src_radius, det_radius, angles, det_px_size=det_px_size, angle_offset=angle_offset, reverse_angles=reverse_angles)

    proj_geom = astra.create_proj_geom('fanflat_vec', det_shape, vectors)
    return proj_geom


# ray transform
class ASTRARayTrafoFanBeam(RayTrafo):
    def __init__(self, im_shape, src_radius, det_radius, angles, det_shape, im_px_size=None, det_px_size=None, vec=True, vol_indexing='yx', proj_indexing='au', det_axis_orient_like_odl=True):
        """
        Fan-beam ray transform
        (with the rotation axis being orthogonal to the xy plane and in the center
        of the volume).

        Parameters
        ----------
        im_shape : tuple of int
            Volume shape, a permutation of `(n_y, n_x)` depending on
            :param:`vol_indexing`.
        src_radius : float
            Source radius (distance between source and origin).
        det_radius : float
            Detector radius (distance between detector and origin).
        angles : array-like
            Rotation angles at which to compute projections.
        det_shape : int
            Number of detector pixels (columns).
        im_px_size : float or 2-tuple of float, optional
            Image voxel size; if a 2-tuple is specified, the dimension order
            should be as specified by :param:`vol_indexing` (i.e., matching the
            dimension order of :param:`im_shape` and :param:`x` passed to
            :meth:`apply`).
            If `None` or a scalar, the tuple is determined by
            :func:`UtilsASTRA.get_default_im_shape_and_px_size_2d`.
        det_px_size : float, optional
            Detector pixel size (column spacing). If `None` (the default),
            it is determined by :func:`get_default_det_px_size_fan_beam`, with
            `xy_diagonal` being calculated from ``x.shape`` and :param:`im_px_size`.
        vec : bool or array, optional
            If a bool, whether to use the `'fanflat_vec'` or the `'fanflat'` projection
            routine; if an array, specifies the vectors for a custom `'fanflat_vec'`
            geometry.
            The default is `True`, i.e. the standard geometry via `'fanflat_vec'`.
        vol_indexing : str, optional
            Volume dimension order, must be either `'yx'` (the default) or `'xy'`.
            If ``vec`` is a bool (i.e. for the standard geometry), no explicit
            transposition is performed, instead operating on `'xy'` data is
            implemented by adapting the angles internally and flipping the
            obtained projections along the horizontal detector axis, or
            conversely leaving them flipped if ``det_axis_orient_like_odl=True``
            (cf. ODL's ASTRA backend).
            If ``vec`` is an array, explicit transposition is performed.
        proj_indexing : str, optional
            Projection dimension order to which the output should be converted.
            Must be either `'au'` (the default) or `'ua'`. `'a'` denotes angles and
            `'u'` denotes detector columns.
        det_axis_orient_like_odl : bool, optional
            If `True` (the default), the projections produced by ASTRA are
            flipped along the detector axis (or conversely are left flipped if
            ``vec`` is a bool and ``vol_indexing='xy'``), matching the behaviour
            of ODL.
        """
        self.im_shape = im_shape
        self.angles = angles
        self.det_shape = det_shape

        self.num_angles = len(self.angles)

        if np.isscalar(vec):
            vec = bool(vec)
        else:
            vec = np.asarray(vec)

        self.is_default_geometry = isinstance(vec, bool)

        self.astra_proj_shape = (self.num_angles, self.det_shape)

        self.vol_indexing = vol_indexing

        if self.is_default_geometry:  # angle adaptation trick for implicit xy transposition is applicable
            # determine working vol_indexing order
            working_vol_indexing = self.vol_indexing
        else:
            ## convert volume data to ASTRA's native yx order
            working_vol_indexing = 'yx'

        if self.is_default_geometry:
            # use angles to adapt to different image coordinates (avoid transposing the array)
            if working_vol_indexing == 'xy':
                # use case A) described in get_fan_beam_proj_geom()
                self._angle_offset = 1.5 * np.pi
                self._reverse_angles = False
                self._flip_det_horz = True
            else:  # 'yx':
                # use case B) described in get_fan_beam_proj_geom()
                self._angle_offset = np.pi
                self._reverse_angles = True
                self._flip_det_horz = False
        else:  # custom geometry
            self._flip_det_horz = False

        if det_axis_orient_like_odl:
            self._flip_det_horz = not self._flip_det_horz

        self.proj_indexing = proj_indexing

        self.proj_shape = [
                self.astra_proj_shape['au'.index(d)]
                for d in self.proj_indexing]

        # reorder shape and px sizes from input vol_indexing order to working_vol_indexing
        self.astra_im_shape = [
                self.im_shape[self.vol_indexing.index(d)]
                for d in working_vol_indexing]
        if im_px_size is None or np.isscalar(im_px_size):
            astra_im_px_size = im_px_size
        else:
            astra_im_px_size = [
                    im_px_size[self.vol_indexing.index(d)]
                    for d in working_vol_indexing]
        _, astra_im_px_size = get_default_im_shape_and_px_size_2d(self.astra_im_shape, im_px_size=astra_im_px_size)
        # create volume geometry
        self.vol_geom = get_2d_vol_geom(self.astra_im_shape, im_px_size=astra_im_px_size)

        xy_diagonal = get_xy_diagonal(self.astra_im_shape, astra_im_px_size)
        if det_px_size is None:
            det_px_size = get_default_det_px_size_fan_beam(src_radius, det_radius, self.det_shape, xy_diagonal=xy_diagonal)
        # create projection geometry
        if vec is True:
            self.proj_geom = get_fan_beam_vec_proj_geom(src_radius, det_radius, self.angles, self.det_shape, det_px_size=det_px_size, angle_offset=self._angle_offset, reverse_angles=self._reverse_angles)
        elif vec is False:
            self.proj_geom = get_fan_beam_proj_geom(src_radius, det_radius, self.angles, self.det_shape, det_px_size=det_px_size, angle_offset=self._angle_offset, reverse_angles=self._reverse_angles)
        else:  # vec is a manually specified array
            self.proj_geom = astra.create_proj_geom('fanflat_vec', self.astra_proj_shape[1], vec)

        if TORCH_AVAILABLE:
            self.create_torch_modules()

    def create_torch_modules(self):
        self.fp_module = TorchLinkedASTRARayTrafoModule(
                self.vol_geom, self.proj_geom, adjoint=False)
        self.bp_module = TorchLinkedASTRARayTrafoModule(
                self.vol_geom, self.proj_geom, adjoint=True)

    def apply(self, x):
        """
        Parameters
        ----------
        x : array-like
            Volume data, shape `(n_y, n_x)` or `(n_x, n_y)` depending on
            :attr:`vol_indexing`.

        Returns
        -------
        y : :class:`np.ndarray`
            Projection data, shape `(angles, det_cols)` or
            `(det_cols, angles)` depending on :attr:`proj_indexing`.
        """
        if not self.is_default_geometry and self.vol_indexing == 'xy':
            # convert to yx explicitly
            x = np.transpose(x, axes=[1, 0])
        x = np.ascontiguousarray(x, dtype=np.float32)

        projs = np.zeros(self.astra_proj_shape, dtype=np.float32)

        # run forward projection
        with AstraLinkedFP(self.proj_geom, self.vol_geom, x, projs) as alg_id:
            astra.algorithm.run(alg_id)

        proj_transpose_axes = ['au'.index(d) for d in self.proj_indexing]  # determine tranposition to obtain requested coordinates from au coordinates produced by ASTRA

        projs = np.transpose(projs, axes=proj_transpose_axes)
        if self._flip_det_horz:
            projs = np.flip(projs, axis=self.proj_indexing.index('u'))

        return projs

    def apply_adjoint(self, y):
        """
        Parameters
        ----------
        y : array-like
            Projection data, shape `(angles, det_cols)` or
            `(det_cols, angles)` depending on :attr:`proj_indexing`.

        Returns
        -------
        x : :class:`np.ndarray`
            Volume data, shape `(n_y, n_x)` or `(n_x, n_y)` depending on
            :attr:`vol_indexing`.
        """
        projs = y
        if self._flip_det_horz:
            projs = np.flip(projs, axis=self.proj_indexing.index('u'))

        # determine tranposition to obtain ASTRA coordinates from provided coordinates
        proj_transpose_axes = [self.proj_indexing.index(d) for d in 'au']

        projs = np.ascontiguousarray(np.transpose(projs, axes=proj_transpose_axes), dtype=np.float32)

        x = np.zeros(self.astra_im_shape, dtype=np.float32)

        # run back-projection
        with AstraLinkedBP(self.proj_geom, self.vol_geom, x, projs) as alg_id:
            astra.algorithm.run(alg_id)

        ## convert volume data to requested vol_indexing order
        if not self.is_default_geometry and self.vol_indexing == 'xy':
            # convert from yx to vol_indexing explicitly
            # (working_vol_indexing == 'yx')
            x = np.transpose(x, axes=[1, 0])

        return x

    def apply_torch(self, x):
        """
        Forward project a torch tensor. If `x` requires gradients, they are
        provided via the back-projection (not the exact discrete adjoint).

        Parameters
        ----------
        x : :class:`torch.Tensor`
            Volume data, shape `(n_y, n_x)` or `(n_x, n_y)` depending
            on :attr:`vol_indexing` prepended by an arbitrary number of leading
            dimensions.

        Returns
        -------
        y : :class:`torch.Tensor`
            Projection data, shape `(angles, det_cols)` or
            `(det_cols, angles)` depending on :attr:`proj_indexing`
            prepended by the same leading dimensions as `x`.
        """
        leading_ndim = x.ndim - 2
        ## convert volume data to ASTRA's native yx order
        if not self.is_default_geometry and self.vol_indexing == 'xy':
            # convert to yx explicitly
            vol_transpose_axes = list(range(leading_ndim)) + [
                    leading_ndim + 1, leading_ndim]
            x = torch.permute(x, dims=vol_transpose_axes).float()

        # run forward projection
        projs = self.fp_module(x)

        if self.proj_indexing == 'ua':
            # convert projection data from au coordinates produced by ASTRA to
            # requested ua coordinates
            proj_transpose_axes = list(range(leading_ndim)) + [
                    leading_ndim + 1, leading_ndim]
            projs = torch.permute(projs, dims=proj_transpose_axes)

        if self._flip_det_horz:
            projs = torch.flip(projs, dims=(leading_ndim + self.proj_indexing.index('u'),))

        return projs

    def apply_adjoint_torch(self, y):
        """
        Back-project a torch tensor. If `y` requires gradients, they are
        provided via the forward projection (not the exact discrete adjoint).

        Parameters
        ----------
        y : :class:`torch.Tensor`
            Projection data, shape is a permutation of
            `(angles, det_cols, det_rows)` depending on :attr:`proj_indexing`
            prepended by an arbitrary number of leading dimensions.

        Returns
        -------
        x : :class:`torch.Tensor`
            Volume data, shape is a permutation of `(n_z, n_y, n_x)` depending
            on :attr:`vol_indexing` prepended by the same leading dimensions as
            `y`.
        """
        projs = y
        leading_ndim = projs.ndim - 2

        if self._flip_det_horz:
            projs = torch.flip(projs, dims=(leading_ndim + self.proj_indexing.index('u'),))

        if self.proj_indexing == 'ua':
            # convert projection data to ASTRA's native au coordinates
            proj_transpose_axes = list(range(leading_ndim)) + [
                    leading_ndim + 1, leading_ndim]
            projs = torch.permute(projs, dims=proj_transpose_axes).float()

        # run back-projection
        x = self.bp_module(projs)

        ## convert volume data to requested vol_indexing order
        if not self.is_default_geometry and self.vol_indexing == 'xy':
            # convert from yx to vol_indexing explicitly
            # (working_vol_indexing == 'yx')
            vol_transpose_axes = list(range(leading_ndim)) + [
                    leading_ndim + 1, leading_ndim]
            x = torch.permute(x, dims=vol_transpose_axes)

        return x


# matrix representation
def get_ray_trafo_matrix_fan_beam_astra(im_shape, src_radius, det_radius, angles, det_shape, im_px_size=None, det_px_size=None, flatten=True, order='C', sparse=True, show_pbar=False):
    """
    Return a matrix representing the forward projection with the fan-beam
    geometry (with the rotation center being in the center of the volume).

    The image data dimension order is assumed to be xy; then, the behavior of an
    ODL geometry with ``angles == odl_geometry.angles - 0.5*np.pi`` is matched
    (cf. the documentation of :func:`get_fan_beam_proj_geom`).
    To use angles like :func:`odl.tomo.cone_beam_geometry`,
    pass ``get_angles_fan_beam(..., use_angles_like_odl=True)`` as the
    :param:`angles` argument of this function.

    Parameters
    ----------
    im_shape : int or 2-tuple of int
        Shape of the image (in pixels); if a 2-tuple is specified, it should be
        in xy order.
    src_radius : float
        Source radius (distance between source and origin).
    det_radius : float
        Detector radius (distance between detector and origin).
    angles : array-like
        Rotation angles.
    det_shape : int
        Number of detector pixels (columns).
    im_px_size : float or 2-tuple of float, optional
        Image pixel size; if a 2-tuple is specified, it should be in xy
        order (i.e., matching the dimension order of :param:`x`).
        If `None` or a scalar, the tuple is determined by
        :func:`UtilsASTRA.get_default_im_shape_and_px_size_2d`.
    det_px_size : float, optional
        Detector pixel size (column spacing), passed to
        :func:`get_fan_beam_proj_geom`.
    flatten : bool, optional
        If `True` (the default), return a matrix of shape
        ``(len(angles) * det_shape, im_shape[0] * im_shape[1])``.
        If `'False'`, the matrix is reshaped to
        ``(len(angles), det_shape, im_shape[0], im_shape[1])``.
        Reshaping follows the specified index :param:`order`.
    order : {`'C'`, `'F'`}, optional
        Index order of the collapsed matrix dimensions for ``flatten=True``.
    sparse : bool, optional
        If `True` (the default), return a scipy sparse matrix; otherwise a dense
        numpy array is returned. This option can only be `True` if
        ``flatten=True``.
    """
    if np.isscalar(im_shape):
        im_shape = (im_shape, im_shape)
    det_shape = np.array(det_shape).item()

    num_angles = len(angles)

    proj_shape = (num_angles, det_shape)

    # reorder shape and px sizes from xy (input) order to native ASTRA yx order
    astra_im_shape = (im_shape[1], im_shape[0])
    if im_px_size is None or np.isscalar(im_px_size):
        astra_im_px_size = im_px_size
    else:
        astra_im_px_size = (im_px_size[1], im_px_size[0])
    _, astra_im_px_size = get_default_im_shape_and_px_size_2d(astra_im_shape, im_px_size=astra_im_px_size)
    vol_geom = get_2d_vol_geom(astra_im_shape, im_px_size=astra_im_px_size)
    proj_geom = get_fan_beam_proj_geom(src_radius, det_radius, angles, det_shape, det_px_size=det_px_size)

    x = np.zeros(im_shape, dtype=np.float32)
    projs = np.zeros(proj_shape, dtype=np.float32)

    CHUNK_MEMSIZE = 10000000  # 100MB

    chunk_size = int(CHUNK_MEMSIZE / 4 / np.prod(proj_shape))

    matrix_chunk_list = []

    matrix_chunk = np.zeros((np.prod(proj_shape), chunk_size), dtype='float32')
    cnt = 0

    with AstraLinkedFP(proj_geom, vol_geom, x, projs) as alg_id:
        for i in tqdm(range(np.prod(im_shape)), desc='assembling ray trafo via astra', disable=not show_pbar):
            idx = np.unravel_index(i, im_shape, order=order)
            x[idx] = 1.
            astra.algorithm.run(alg_id)
            x[idx] = 0.
            matrix_chunk[:, i-cnt] = projs.reshape(-1, order=order)
            if (i+1) % chunk_size == 0 or i+1 == np.prod(im_shape):
                num_cols = i+1-cnt
                matrix_chunk_list.append(scipy.sparse.csr_matrix(matrix_chunk[:, :num_cols], copy=True) if sparse else matrix_chunk[:, :num_cols].copy())
                cnt += num_cols
                matrix_chunk[:] = 0.  # re-initialize

    if sparse:
        matrix = scipy.sparse.hstack(matrix_chunk_list).tocsr()
    else:
        matrix = np.hstack(matrix_chunk_list)

    assert matrix.shape[1] == np.prod(im_shape)

    if not flatten:
        matrix = matrix.reshape(proj_shape + im_shape, order=order)

    return matrix

def get_deviating_fan_beam_vectors(vec, src_shifts_xy=None, det_shifts_xy=None, det_shifts_u=None, det_shifts_src_to_det=None):
    """
    Return modified vectors for an ASTRA `'fanflat_vec'` geometry.

    Parameters
    ----------
    src_shifts_xy : array-like, optional
        Source position shift in xy (volume) coordinates.
        Must broadcast to shape ``(len(vec), 2)``.
    det_shifts_xy : array-like, optional
        Detector position shift in xy (volume) coordinates.
        Must broadcast to shape ``(len(vec), 2)``.
    det_shifts_u : array-like, optional
        Detector position shift in u (detector axis) coordinates.
        Shape: ``(len(vec), 1)``, ``(len(vec),)``, ``(1,)`` or ``()``.
    det_shifts_src_to_det : array-like, optional
        Detector position shift in source-to-detector direction.
        Must broadcast to shape ``(len(vec),)``.
    """
    deviating_vec = np.copy(vec)

    if src_shifts_xy is not None:
        deviating_vec[:, 0:2] += src_shifts_xy
    if det_shifts_xy is not None:
        deviating_vec[:, 2:4] += det_shifts_xy
    if det_shifts_u is not None:
        det_shifts_u = np.atleast_1d(det_shifts_u)
        if det_shifts_u.ndim == 1:
            det_shifts_u = det_shifts_u[:, None]
        deviating_vec[:, 2:4] += det_shifts_u * (vec[:, 4:6] / np.sqrt(np.sum(vec[:, 4:6]**2, axis=1)))
    if det_shifts_src_to_det is not None:
        deviating_vec[:, 2:4] += np.asarray(det_shifts_src_to_det) * (vec[:, 2:4] - vec[:, 0:2])

    return deviating_vec

def get_deviating_astra_ray_trafo_fan_beam(
        im_shape, src_radius, det_radius, angles, det_shape,
        deviation_kwargs=None, im_px_size=None, det_px_size=None, vec=None,
        vol_indexing='yx', proj_indexing='au'):
    """
    See the documentation of :meth:`ASTRARayTrafoFanBeam.__init__` for the
    parameters not documented below.

    Parameters
    ----------
    vec : array, optional
        Custom vectors for the non-deviating `'fanflat_vec'` projection geometry.
        If `None`, the default geometry is used as specified by the other
        parameters.
    deviation_kwargs : dict, optional
        Parameters passed to :func:`get_deviating_fan_beam_vectors`, specifying
        the deviation.
    """
    if vec is None:
        # reorder shape and px sizes from xy (input) order to native ASTRA yx order
        astra_im_shape = (im_shape[1], im_shape[0])
        if im_px_size is None or np.isscalar(im_px_size):
            astra_im_px_size = im_px_size
        else:
            astra_im_px_size = (im_px_size[1], im_px_size[0])
        _, astra_im_px_size = get_default_im_shape_and_px_size_2d(astra_im_shape, im_px_size=astra_im_px_size)

        xy_diagonal = get_xy_diagonal(astra_im_shape, astra_im_px_size)
        if det_px_size is None:
            det_px_size = get_default_det_px_size_fan_beam(src_radius, det_radius, det_shape, xy_diagonal=xy_diagonal)

        # set angle_offset and reverse_angles for use case B) described in get_fan_beam_proj_geom()
        vec = get_fan_beam_vectors(src_radius, det_radius, angles, det_px_size, angle_offset=np.pi, reverse_angles=True)

    if deviation_kwargs is None:
        deviation_kwargs = {}

    deviating_vec = get_deviating_fan_beam_vectors(vec, **deviation_kwargs)

    deviating_ray_trafo = ASTRARayTrafoFanBeam(
            im_shape, src_radius, det_radius, angles,
            det_shape, im_px_size=im_px_size,
            det_px_size=det_px_size, vec=deviating_vec,
            vol_indexing=vol_indexing, proj_indexing=proj_indexing,
            det_axis_orient_like_odl=True,  # use case B) described in get_fan_beam_proj_geom()
    )

    return deviating_ray_trafo
